package com.ekotak.projectcategorysearch.repositories;

import com.ekotak.projectcategorysearch.cores.ProjectCategoryCore;

import org.springframework.data.solr.repository.SolrCrudRepository;

public interface ProjectCategoryCoreRepository 
    extends SolrCrudRepository<ProjectCategoryCore, String> 
{
    
}