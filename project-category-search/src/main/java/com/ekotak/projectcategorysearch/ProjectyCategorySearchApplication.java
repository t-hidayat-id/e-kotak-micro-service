package com.ekotak.projectcategorysearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectyCategorySearchApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectyCategorySearchApplication.class, args);
	}

}
