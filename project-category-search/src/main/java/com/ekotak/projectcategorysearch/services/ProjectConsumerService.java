package com.ekotak.projectcategorysearch.services;

import com.ekotak.projectcategory.domain.ProjectCategory;

import com.ekotak.projectcategorysearch.cores.ProjectCategoryCore;
import com.ekotak.projectcategorysearch.repositories.ProjectCategoryCoreRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
class ProjectCategoryConsumerService {
	
	ProjectCategoryCoreRepository projectCategoryCoreRepository;

	@Autowired
	public ProjectCategoryConsumerService(
		ProjectCategoryCoreRepository projectCategoryCoreRepository
	) {
		this.projectCategoryCoreRepository = projectCategoryCoreRepository;
	}
	
    @KafkaListener(topics = "projectCategoryTopic")
	public void processMessage(ProjectCategory projectCategory) {	
		ProjectCategoryCore projectCategoryCore = new ProjectCategoryCore(
			projectCategory.getCategoryId().toString(),  
			projectCategory.getProjectId().toString(),
			projectCategory.getCategoryId().toString() 
		);
		projectCategoryCore.setId(projectCategoryCore.getProjectCategoryId());

		this.projectCategoryCoreRepository.save(projectCategoryCore);
	}

}