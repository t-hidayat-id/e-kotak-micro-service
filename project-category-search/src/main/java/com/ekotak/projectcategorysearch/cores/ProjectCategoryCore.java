package com.ekotak.projectcategorysearch.cores;

import java.io.Serializable;

import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.SolrDocument;

import lombok.Data;

@Data
@SolrDocument(solrCoreName = "projectcategory")
public class ProjectCategoryCore implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
	@Field
    private String id;
    
    @Field("project_category_id")
    String projectCategoryId;

    @Field("project_id")
    String projectId;

    @Field("category_id")
    String categoryId;

    public ProjectCategoryCore() {}

    public ProjectCategoryCore(
        String projectCategoryId, 
        String projectId,
        String categoryId
    ) {
       this.projectCategoryId = projectCategoryId;
       this.projectId = projectId;
       this.categoryId = categoryId;
    }

}