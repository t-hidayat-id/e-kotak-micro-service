package com.ekotak.projectcategory.domain;

import java.io.Serializable;
import java.util.UUID;

import lombok.Data;

@Data
public class ProjectCategory implements Serializable {

    private static final long serialVersionUID = -1440143802505688103L;

    UUID projectCategoryId;
    UUID projectId;
    UUID categoryId;

    public ProjectCategory() {};

    public ProjectCategory(UUID projectCategoryId, UUID projectId, UUID categoryId) {
        this.projectCategoryId = projectCategoryId;
        this.projectId = projectId;
        this.categoryId = categoryId;
    }

}