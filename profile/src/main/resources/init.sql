CREATE TABLE IF NOT EXISTS ekotak_profile.profile (
    profile_id timeuuid PRIMARY KEY,
    user_id timeuuid,
    link_id text,
    display_name text,
    description text
);
CREATE TABLE IF NOT EXISTS ekotak_profile.profile_by_user (
    user_id timeuuid PRIMARY KEY,
    profile_id timeuuid
);
CREATE TABLE IF NOT EXISTS ekotak_profile.profile_by_link (
    link_id text PRIMARY KEY,
    profile_id timeuuid
);