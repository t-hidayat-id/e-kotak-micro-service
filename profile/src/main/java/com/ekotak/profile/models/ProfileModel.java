package com.ekotak.profile.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.ekotak.profile.validators.IsAvailableLinkId;

import lombok.Data;

@Data
public class ProfileModel {

    @NotNull(message = "Link cannot be null!")
    @Size(
        min = 10, 
        max = 50,
        message = "Link ID must be equal or greater than 10 characters" + 
            " and less than 50 characters!"
    )
    @IsAvailableLinkId
    private String linkId;

    @NotNull(message="Display name cannot be null!")
    @Size(
        min = 10, 
        max = 50,
        message = "Display name must be equal or greater than 8 characters" + 
            " and less than 50 characters!"
    )
    private String displayName;

    @Size(
        min = 30, 
        max = 100,
        message = "Description must be equal or greater than 30 characters" + 
            " and less than 100 characters!"
    )
    private String description;

}