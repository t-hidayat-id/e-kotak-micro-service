package com.ekotak.profile.domain;

import java.io.Serializable;
import java.util.UUID;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import lombok.Data;

@Data
@Table("profile_by_user")
public class ProfileByUser implements Serializable {
     
    private static final long serialVersionUID = -7114936679578594878L;

    @PrimaryKey("user_id")
    private UUID userId;

    @Column("profile_id")
    private UUID profileId;
   
    public ProfileByUser() {
    
    }

    public ProfileByUser(UUID userId, UUID profileId) {
        this.userId = userId;
        this.profileId = profileId;
    }

    @Override
	public String toString() {
        return String.format(
            "ProfileByUser[userId='%s', profileId=%s]", 
            this.userId, this.profileId
        );
    }

}