package com.ekotak.profile.domain;

import java.io.Serializable;
import java.util.UUID;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import lombok.Data;

@Data
@Table("profile")
public class Profile implements Serializable {
    
    private static final long serialVersionUID = 6988393509456545390L;
    
    @PrimaryKey("profile_id")
    private UUID profileId;

    @Column("user_id")
    private UUID userId;

    @Column("display_name")
    private String displayName;

    @Column("link_id")
    private String linkId;

    @Column("description")
    private String description;

    public Profile() {
    
    }

    public Profile(
        UUID profileId, 
        UUID userId, 
        String linkId, 
        String diplayName, 
        String description
    ) {
        this.profileId = profileId;
        this.userId = userId;
        this.linkId = linkId;
        this.displayName = diplayName;
        this.description = description;
    }

    @Override
	public String toString() {
        return String.format(
            "Profile[profileId='%s', " + 
            "userId=%s, " + 
            "linkId=%s, " + 
            "displayName=%s, " + 
            "description=%s]", 
            this.profileId, 
            this.userId, 
            this.linkId, 
            this.displayName, 
            this.description
        );
    }

}