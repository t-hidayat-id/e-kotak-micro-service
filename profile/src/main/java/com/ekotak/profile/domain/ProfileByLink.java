package com.ekotak.profile.domain;

import java.io.Serializable;
import java.util.UUID;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import lombok.Data;

@Data
@Table("profile_by_link")
public class ProfileByLink implements Serializable {

    private static final long serialVersionUID = 3516403035411980978L;

    @PrimaryKey("link_id")
    private String linkId;

    @Column("profile_id")
    private UUID profileId;
   
    public ProfileByLink() {}

    public ProfileByLink(String linkId, UUID profileId) {
        this.linkId = linkId;
        this.profileId = profileId;
    }

    @Override
	public String toString() {
        return String.format(
            "ProfileByLink[linkId='%s', profileId=%s]", 
            this.linkId, this.profileId
        );
    }

}