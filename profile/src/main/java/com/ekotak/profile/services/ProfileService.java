package com.ekotak.profile.services;

import java.util.UUID;

import com.ekotak.profile.domain.Profile;

public interface ProfileService {

    Profile create(Profile profile);
    Profile findByUserId(UUID userId);
    Profile update(Profile profile);

}