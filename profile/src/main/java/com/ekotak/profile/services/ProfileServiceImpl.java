package com.ekotak.profile.services;

import java.util.UUID;

import com.ekotak.profile.domain.Profile;
import com.ekotak.profile.domain.ProfileByUser;
import com.ekotak.profile.domain.ProfileByLink;
import com.ekotak.profile.exceptions.ProfileNotFoundException;
import com.ekotak.profile.exceptions.UserProfileDuplicateException;
import com.ekotak.profile.repositories.ProfileByUserRepository;
import com.ekotak.profile.repositories.ProfileByLinkRepository;
import com.ekotak.profile.repositories.ProfileRepository;

import com.fasterxml.uuid.Generators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class ProfileServiceImpl implements ProfileService {

    @Autowired
    ProfileByUserRepository profileByUserRepository;

    @Autowired
    ProfileByLinkRepository profileByLinkRepository;

    @Autowired
    ProfileRepository profileRepository;
    
    /**
     * This method to create a profile and related record to it. 
     * First we check if the user already has profile, 
     * our rules is one user only has one profile. And then save 
     * the profile to table "profile_by_user" after that save it in the table 
     * "profile_by_username" with a lowercase "username". And lastly save data 
     * to the table "profile". Note: all hree table has related to each other 
     * with field "id".  
     */
    @Override
    public Profile create(Profile profile) {
        UUID userId = UUID.fromString(
            SecurityContextHolder.getContext().getAuthentication().getName()
        );
        
        ProfileByUser profileByUser = profileByUserRepository.findByUserId(
            userId
        ).orElse(null);
        
        if (profileByUser != null) {
            throw new UserProfileDuplicateException(userId);
        }

        UUID profileId = Generators.timeBasedGenerator().generate();
        
        ProfileByUser newProfileByUser = new ProfileByUser(userId, profileId);
        profileByUserRepository.save(newProfileByUser);

        ProfileByLink newProfileByLink = new ProfileByLink(
            profile.getLinkId().toLowerCase(), profileId
        );
        profileByLinkRepository.save(newProfileByLink);

        profile.setProfileId(profileId);
        profile.setUserId(userId);
        profileRepository.save(profile);
        
        return profile;
    }

    /**
     * This method to update two record related to profile. 
     * First find a record in "ProfileByUser" which have related 
     * to the login user and update a record 
     * in the "Profile" and then delete a related record 
     * in the "ProfileByUsername" because we will update a primary key value.
     * And primary can not change to a new one, solutin just delete it 
     * and make a new one.
     */
    @Override
    public Profile findByUserId(UUID userId) {
        ProfileByUser profileByUser = profileByUserRepository.findByUserId(
            userId
        ).orElseThrow(() -> new ProfileNotFoundException(userId));

        return profileRepository.findById(
            profileByUser.getProfileId()
        ).orElse(null);
    }

    @Override
    public Profile update(Profile updateProfile) {
        UUID userId = UUID.fromString(
            SecurityContextHolder.getContext().getAuthentication().getName()
        );
        
        ProfileByUser profileByUser = profileByUserRepository.findByUserId(
            userId
        ).orElseThrow(() -> new ProfileNotFoundException(userId));

        Profile profile = profileRepository.findById(
            profileByUser.getProfileId()
        ).orElse(null);

        ProfileByLink profileByLink = 
            profileByLinkRepository.findByLinkId(
                profile.getLinkId().toLowerCase()
        ).orElse(null);
        
        profile.setLinkId(updateProfile.getLinkId().toLowerCase());
        profile.setDisplayName(updateProfile.getDisplayName());
        profile.setDescription(updateProfile.getDescription());
        profileRepository.save(profile);

        profileByLinkRepository.delete(profileByLink);

        profileByLink = new ProfileByLink();
        profileByLink.setProfileId(profile.getProfileId());
        profileByLink.setLinkId(updateProfile.getLinkId().toLowerCase());
        profileByLinkRepository.save(profileByLink);
        
        return profile;
    }
    
}