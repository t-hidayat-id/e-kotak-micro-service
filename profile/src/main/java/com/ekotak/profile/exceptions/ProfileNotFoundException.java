package com.ekotak.profile.exceptions;

import java.util.UUID;

public class ProfileNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private UUID userId;

    public ProfileNotFoundException() {

    }

    public ProfileNotFoundException(UUID userId) {
        super("Profile is not found.");
        
        this.userId = userId;
    }

    public UUID getUserId() {
        return this.userId;
    }

}