package com.ekotak.profile.exceptions;

import java.util.UUID;

public class UserProfileDuplicateException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private UUID id;

    public UserProfileDuplicateException() {
        
    }

    public UserProfileDuplicateException(UUID id) {
        super("This user already has a profile! One user only has one profile.");
        
        this.id = id;
    }

    public UUID getId() {
        return this.id;
    }

}