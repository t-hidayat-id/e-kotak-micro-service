package com.ekotak.profile.validators;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Pattern;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({FIELD})
@Constraint(validatedBy={})
@Retention(RUNTIME)
@Pattern(regexp=
    "^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$", 
    flags = Pattern.Flag.CASE_INSENSITIVE
)
public @interface IsUUID {

    String message() default "UUID is not valid!";
    
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}