package com.ekotak.profile.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = IsAvailableLinkIdValidator.class)
@Documented
public @interface IsAvailableLinkId {
    
    String message() default "The profile link is already taken!";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}