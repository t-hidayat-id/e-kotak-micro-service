package com.ekotak.profile.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.ekotak.profile.domain.Profile;
import com.ekotak.profile.domain.ProfileByLink;
import com.ekotak.profile.repositories.ProfileByLinkRepository;
import com.ekotak.profile.repositories.ProfileRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

public class IsAvailableLinkIdValidator 
    implements ConstraintValidator<IsAvailableLinkId, String> 
{
    
    @Autowired 
    ProfileByLinkRepository profileByLinkRepository; 
    
    @Autowired
    ProfileRepository profileRepository;

    @Override
    public boolean isValid(
        String linkId, ConstraintValidatorContext context) 
    {
        ProfileByLink profileByLink = 
            profileByLinkRepository.findByLinkId(linkId).orElse(null);
        
        if (profileByLink == null)
            return true;

        Profile profile = profileRepository.findById(
            profileByLink.getProfileId()
        ).orElse(null);

        if (profile != null) {
            String userId = SecurityContextHolder.getContext()
                .getAuthentication().getName();
            
            if (profile.getUserId().toString().equals(userId)) 
                return true;
        }
        
        return false;
    }

}