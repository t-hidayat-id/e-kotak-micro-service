package com.ekotak.profile.controllers;

import java.util.UUID;

import javax.validation.Valid;

import com.ekotak.profile.domain.Profile;
import com.ekotak.profile.models.ProfileModel;
import com.ekotak.profile.services.ProfileService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/profiles")
public class ProfileController {

    @Autowired
    ProfileService profileService;

    @PostMapping(
        consumes = { 
            MediaType.APPLICATION_XML_VALUE, 
            MediaType.APPLICATION_JSON_VALUE 
        },
		produces = { 
            MediaType.APPLICATION_XML_VALUE, 
            MediaType.APPLICATION_JSON_VALUE 
        }
	)
    public ResponseEntity<Object> create(
        @Valid @RequestBody ProfileModel profileModel) 
    {
        Profile profile = new Profile();
        profile.setLinkId(profileModel.getLinkId());
        profile.setDisplayName(profileModel.getDisplayName());
        profile.setDescription(profileModel.getDescription());
        
        Profile newProfile = profileService.create(profile);

		return ResponseEntity.status(HttpStatus.CREATED).body(newProfile);
    }

    @GetMapping(
        value="/{userId}", 
        consumes = { 
            MediaType.APPLICATION_XML_VALUE, 
            MediaType.APPLICATION_JSON_VALUE 
        }
    )
    public ResponseEntity<Object> 
        findByUserId(@PathVariable("userId") UUID userId)
    {
        Profile profile = profileService.findByUserId(userId); 
        
        return ResponseEntity.status(HttpStatus.OK).body(profile);
    }

    @PutMapping(
        consumes = { 
            MediaType.APPLICATION_XML_VALUE, 
            MediaType.APPLICATION_JSON_VALUE 
        },
		produces = { 
            MediaType.APPLICATION_XML_VALUE, 
            MediaType.APPLICATION_JSON_VALUE 
        }
	)
    public ResponseEntity<Object> update(
        @Valid @RequestBody ProfileModel profileModel) 
    {
        Profile profile = new Profile();
        profile.setLinkId(profileModel.getLinkId());
        profile.setDisplayName(profileModel.getDisplayName());
        profile.setDescription(profileModel.getDescription());

        Profile updateProfile = profileService.update(profile);

		return ResponseEntity.status(HttpStatus.CREATED).body(updateProfile);
    }
    
}