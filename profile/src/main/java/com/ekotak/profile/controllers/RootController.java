package com.ekotak.profile.controllers;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.http.ResponseEntity;

@RestController
public class RootController {

    RootController() {
    }

    @GetMapping("/")
    ResponseEntity<ResourceSupport> root() {
        ResourceSupport resourceSupport = new ResourceSupport();

        resourceSupport.add(
            linkTo(methodOn(RootController.class).root()).withSelfRel()
        );

        resourceSupport.add(
            linkTo(methodOn(ProfileController.class).create(null))
            .withRel("create-profile")
        );

        resourceSupport.add(
            linkTo(methodOn(ProfileController.class).findByUserId(null))
            .withRel("find-profile-by-user-id")
        );

        resourceSupport.add(
            linkTo(methodOn(ProfileController.class).update(null))
            .withRel("update-profile")
        );
        
        return ResponseEntity.ok(resourceSupport);
    }
}