package com.ekotak.profile.repositories;

import java.util.Optional;
import java.util.UUID;

import com.ekotak.profile.domain.ProfileByUser;

import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface ProfileByUserRepository extends 
    CrudRepository<ProfileByUser, UUID> 
{

    @Query("SELECT * FROM profile_by_user WHERE user_id=?0")
    Optional<ProfileByUser> findByUserId(UUID userId);
     
}