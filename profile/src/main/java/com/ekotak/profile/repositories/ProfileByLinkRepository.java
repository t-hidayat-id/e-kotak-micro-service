package com.ekotak.profile.repositories;

import java.util.Optional;

import com.ekotak.profile.domain.ProfileByLink;

import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface ProfileByLinkRepository 
    extends CrudRepository<ProfileByLink, String> 
{

    @Query("SELECT * FROM profile_by_link WHERE link_id=?0")
    Optional<ProfileByLink> findByLinkId(String linkId); 

}