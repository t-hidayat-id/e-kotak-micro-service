package com.ekotak.profile.repositories;

import java.util.UUID;

import com.ekotak.profile.domain.Profile;

import org.springframework.data.repository.CrudRepository;

public interface ProfileRepository extends CrudRepository<Profile, UUID> {

} 