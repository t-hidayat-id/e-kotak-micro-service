CQL="CREATE KEYSPACE IF NOT EXISTS ekotak_user WITH replication = {'class': 'SimpleStrategy', 'replication_factor': '1'};
CREATE TABLE IF NOT EXISTS ekotak_user.user (
    id timeuuid PRIMARY KEY,
    email text,
    encryptedpassword text,
    firstname text,
    lastname text
);
CREATE TABLE IF NOT EXISTS ekotak_user.user_by_email (
    email text PRIMARY KEY,
    id timeuuid
);
CREATE KEYSPACE IF NOT EXISTS ekotak_profile WITH replication = {'class': 'SimpleStrategy', 'replication_factor': '1'};
CREATE TABLE IF NOT EXISTS ekotak_profile.profile (
    profile_id timeuuid PRIMARY KEY,
    user_id timeuuid,
    link_id text,
    display_name text,
    description text
);
CREATE TABLE IF NOT EXISTS ekotak_profile.profile_by_user (
    user_id timeuuid PRIMARY KEY,
    profile_id timeuuid
);
CREATE TABLE IF NOT EXISTS ekotak_profile.profile_by_link (
    link_id text PRIMARY KEY,
    profile_id timeuuid
);
"

until echo $CQL | cqlsh; do
  echo "cqlsh: Cassandra is unavailable to initialize - will retry later"
  sleep 2
done &

exec /docker-entrypoint.sh "$@"