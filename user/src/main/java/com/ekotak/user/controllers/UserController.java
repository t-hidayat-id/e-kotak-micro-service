package com.ekotak.user.controllers;

import java.util.UUID;

import javax.validation.Valid;

import com.ekotak.user.domain.User;
import com.ekotak.user.models.CreateUserRequestModel;
import com.ekotak.user.models.CreateUserResponseModel;
import com.ekotak.user.models.UserResponseModel;
import com.ekotak.user.services.UserService;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    Environment environment;

    @Autowired
	UserService userService;

    @PostMapping(
        consumes = { 
            MediaType.APPLICATION_XML_VALUE, 
            MediaType.APPLICATION_JSON_VALUE 
        },
		produces = { 
            MediaType.APPLICATION_XML_VALUE, 
            MediaType.APPLICATION_JSON_VALUE 
        }
	)
    public ResponseEntity<CreateUserResponseModel> createUser(
        @Valid @RequestBody CreateUserRequestModel createUserRequestModel) 
    {
		ModelMapper modelMapper = new ModelMapper(); 
		modelMapper.getConfiguration().setMatchingStrategy(
            MatchingStrategies.STRICT
        );
		
        User user = modelMapper.map(createUserRequestModel, User.class);
        
        user.setEncryptedPassword(createUserRequestModel.getPassword());
		
		User createdUser = userService.createUser(user);
		CreateUserResponseModel returnValue = modelMapper.map(
            createdUser, CreateUserResponseModel.class
        );
		
		return ResponseEntity.status(HttpStatus.CREATED).body(returnValue);
    }
    
    @GetMapping(
        value="/{id}", 
        produces = { 
            MediaType.APPLICATION_XML_VALUE, 
            MediaType.APPLICATION_JSON_VALUE 
        }
    )
    public ResponseEntity<UserResponseModel> 
        getUser(@PathVariable("id") UUID id) 
    {  
        User user = userService.getById(id); 
        
        UserResponseModel returnValue = new ModelMapper().map(
            user, UserResponseModel.class
        );
        
        return ResponseEntity.status(HttpStatus.OK).body(returnValue);
    }
    
}