package com.ekotak.user.controllers;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.http.ResponseEntity;

@RestController
public class RootController {

    RootController() {
    }

    @GetMapping("/")
    ResponseEntity<ResourceSupport> root() {
        ResourceSupport resourceSupport = new ResourceSupport();

        resourceSupport.add(
            linkTo(methodOn(RootController.class).root()).withSelfRel()
        );

        resourceSupport.add(
            linkTo(methodOn(
                UserController.class).createUser(null)
            ).withRel("create-user")
        );

        resourceSupport.add(
            linkTo(methodOn(
                UserController.class).getUser(null)
            ).withRel("get-user")
        );

        resourceSupport.add(
            linkTo(methodOn(
                UserController.class).createUser(null)
            ).slash("login").withRel("login-user")
        );
        
        return ResponseEntity.ok(resourceSupport);
    }
}