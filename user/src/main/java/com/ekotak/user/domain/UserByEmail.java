package com.ekotak.user.domain;

import java.io.Serializable;
import java.util.UUID;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import lombok.Data;

@Data
@Table("user_by_email")
public class UserByEmail implements Serializable {

    private static final long serialVersionUID = 477216309885418670L;

    @PrimaryKey
    private String email;
    
    private UUID id;

    public UserByEmail() {
    }

    public UserByEmail(String email, UUID id) {
        this.email = email;
        this.id = id;
    }

    @Override
	public String toString() {
		return String.format("UserByEmail[email=%s, id='%s']", this.email, this.id);
    }
    
}