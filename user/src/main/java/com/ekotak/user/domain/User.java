package com.ekotak.user.domain;

import java.io.Serializable;
import java.util.UUID;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import lombok.Data;

@Data
@Table("user")
public class User implements Serializable {

    private static final long serialVersionUID = -4349504587514801138L;

    @PrimaryKey
    private UUID id;

    private String email;
    private String encryptedPassword;
    private String firstName;
    private String lastName;
    
    public User() {
        
    }

    public User(UUID id, String email, String encryptedPassword, 
        String firstName, String lastName) {

        this.id = id;
        this.email = email;
        this.encryptedPassword = encryptedPassword;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
	public String toString() {

		return String.format(
            "User[id='%s', email=%s, firstName=$s, lastName=$s]", 
            this.id, this.email, this.firstName, this.lastName);
    }
    
}