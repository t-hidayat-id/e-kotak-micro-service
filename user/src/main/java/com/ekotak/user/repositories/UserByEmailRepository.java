package com.ekotak.user.repositories;

import java.util.UUID;

import com.ekotak.user.domain.UserByEmail;

import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface UserByEmailRepository extends CrudRepository<UserByEmail, UUID> 
{

    @Query("SELECT * FROM user_by_email WHERE email=?0")
    UserByEmail findByEmail(String email);  
      
}