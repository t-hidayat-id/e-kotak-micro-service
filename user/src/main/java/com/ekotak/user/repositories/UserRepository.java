package com.ekotak.user.repositories;

import java.util.UUID;

import com.ekotak.user.domain.User;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, UUID> {

}