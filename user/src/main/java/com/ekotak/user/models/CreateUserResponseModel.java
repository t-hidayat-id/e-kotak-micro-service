package com.ekotak.user.models;

import java.util.UUID;

import lombok.Data;

@Data
public class CreateUserResponseModel {

    private String firstName;
	private String lastName;
	private String email;
	private UUID id;

}