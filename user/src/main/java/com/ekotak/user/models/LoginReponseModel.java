package com.ekotak.user.models;

import lombok.Data;

@Data
public class LoginReponseModel {

    private String id;
    private String jwt;
    
    public LoginReponseModel(String id, String jwt) {
        this.id =  id;
        this.jwt = jwt;
    }

}