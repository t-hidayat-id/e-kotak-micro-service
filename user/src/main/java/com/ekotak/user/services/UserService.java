package com.ekotak.user.services;

import java.util.UUID;

import com.ekotak.user.domain.User;

import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    User createUser(User user);
    User getById(UUID id);
    User getByEmail(String email);
    
}