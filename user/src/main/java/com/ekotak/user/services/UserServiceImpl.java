package com.ekotak.user.services;

import java.util.ArrayList;
import java.util.UUID;

import com.ekotak.user.domain.User;
import com.ekotak.user.domain.UserByEmail;
import com.ekotak.user.exceptions.UserNotFoundException;
import com.ekotak.user.repositories.UserByEmailRepository;
import com.ekotak.user.repositories.UserRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.fasterxml.uuid.Generators;

@Service
public class UserServiceImpl implements UserService {

    UserRepository userRepository;
    UserByEmailRepository userByEmailRepository;
    BCryptPasswordEncoder bCryptPasswordEncoder;
    Environment environment;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public UserServiceImpl(
            UserRepository userRepository, 
            UserByEmailRepository userByEmailRepository,
            BCryptPasswordEncoder bCryptPasswordEncoder, 
            Environment environment) 
    {
        this.userRepository = userRepository;
        this.userByEmailRepository = userByEmailRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.environment = environment;
    }

    @Override
    public User createUser(User user) {
        UUID uuid = Generators.timeBasedGenerator().generate();

        UserByEmail userByEmail = new UserByEmail(user.getEmail(), uuid);
        userByEmailRepository.save(userByEmail);

        user.setId(uuid);
        user.setEncryptedPassword(bCryptPasswordEncoder.encode(
            user.getEncryptedPassword()
        ));
        userRepository.save(user);

        return user;
    }

    @Override
    public UserDetails loadUserByUsername(String username) 
        throws UsernameNotFoundException 
    {
        UserByEmail userByEmail = userByEmailRepository.findByEmail(username);

        if (userByEmail == null) throw new UsernameNotFoundException(username);	

        User user = userRepository.findById(userByEmail.getId()).orElse(null);
        
		return new org.springframework.security.core.userdetails.User(
            user.getEmail(), user.getEncryptedPassword(), 
            true, true, true, true, new ArrayList<>()
        );
    }
    
    @Override 
    public User getById(UUID id) {
        User user = userRepository.findById(id).orElse(null);

        if(user == null) throw new UserNotFoundException(id);

        return user;
    }

    @Override 
    public User getByEmail(String email) {
        UserByEmail userByEmail = userByEmailRepository.findByEmail(email);

        if (userByEmail == null) throw new UserNotFoundException(email);

        User user = userRepository.findById(userByEmail.getId()).orElse(null);

        return user;
    }

}
