package com.ekotak.user.exceptions;

import java.util.UUID;


public class UserNotFoundException extends RuntimeException {
    
    private static final long serialVersionUID = 1L;

    private UUID id;
    private String email;

    public UserNotFoundException() {    
            
    }

    public UserNotFoundException(UUID id) {
        super("User is not found!");

        this.id = id;
    }

    public UserNotFoundException(String email) {
        super("User is not found!");

        this.email = email;
    }

    public UUID getId() {
        return this.id;
    }

    public String getEmail() {
        return this.email;
    }

}