package com.ekotak.user.exceptions;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation
    .ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler 
    extends ResponseEntityExceptionHandler 
{

	@Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
        MethodArgumentNotValidException ex,
        HttpHeaders headers,
        HttpStatus status, WebRequest request) 
    {
		Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", new Date());
        body.put("status", status.name());

        Map<String, String> errors = new LinkedHashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });

        body.put("errors", errors);

        return new ResponseEntity<>(body, headers, status);
    }

    @ExceptionHandler(value = {UserNotFoundException.class})
    protected ResponseEntity<Object> profileTypeNotFound(
        UserNotFoundException ex, WebRequest request) 
    {
        List<Object> errorList = new ArrayList<Object>();
        HashMap<String, String> error = new HashMap<String, String>();
         
        String fieldName = ex.getId() != null ? "id" : "email";
        String fieldvalue = ex.getId() != null ? 
            ex.getId().toString() : ex.getEmail();
            
        error.put(fieldName, fieldvalue);
        errorList.add(error);

        RestException restException = 
            new RestException(
                HttpStatus.NOT_FOUND, 
                ex.getLocalizedMessage(), 
                errorList
            );

        return handleExceptionInternal(ex, restException, 
          new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

}