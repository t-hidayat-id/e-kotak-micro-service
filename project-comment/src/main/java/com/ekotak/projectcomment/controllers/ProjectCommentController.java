package com.ekotak.projectcomment.controllers;

import javax.validation.Valid;

import com.ekotak.projectcomment.domain.ProjectComment;
import com.ekotak.projectcomment.models.ProjectCommentPostModel;
import com.ekotak.projectcomment.services.ProjectCommentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/project-comments")
public class ProjectCommentController {

    @Autowired
    ProjectCommentService projectCommentService;

    @PostMapping(
        consumes = { 
            MediaType.APPLICATION_JSON_VALUE 
        },
		produces = { 
            MediaType.APPLICATION_JSON_VALUE 
        }
	)
    public ResponseEntity<Object> create(
        @Valid @RequestBody ProjectCommentPostModel projectCommentPostModel
    ) {
        ProjectComment projectComment = new ProjectComment(
            null, 
            projectCommentPostModel.getProjectId(),
            null, 
            projectCommentPostModel.getComment());

        return ResponseEntity.status(HttpStatus.CREATED).body(
            projectCommentService.create(projectComment)
        );
    }
}