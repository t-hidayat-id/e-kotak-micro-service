package com.ekotak.projectcomment.exceptions;

public class ProfileNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 3154394285601414270L;
    
    private String userId;

    public ProfileNotFoundException() {

    }

    public ProfileNotFoundException(String userId) {
        super("Profile of the current user is not found.");
        
        this.userId = userId;
    }

    public String getUserId() {
        return this.userId;
    }

}