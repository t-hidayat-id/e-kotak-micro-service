package com.ekotak.projectcomment.models;

import java.util.UUID;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class ProjectCommentPostModel {

    @NotNull(message = "Project Id cannot be null.")
    UUID projectId;

    @NotNull(message = "Comment Id cannot be null.")
    String comment;

}