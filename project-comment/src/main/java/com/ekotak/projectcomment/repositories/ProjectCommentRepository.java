package com.ekotak.projectcomment.repositories;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.ekotak.projectcomment.domain.ProjectComment;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectCommentRepository extends CrudRepository<ProjectComment, UUID> {
   
    Optional<ProjectComment> findByProjectCommentId(UUID projectCommentId);
    List<ProjectComment> findAll(); 

}