package com.ekotak.projectcomment.domain;

import java.io.Serializable;
import java.util.UUID;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import lombok.Data;

@Data
@Table("project_comment")
public class ProjectComment implements Serializable {

    private static final long serialVersionUID = -6055299955950900198L;

    @PrimaryKey("project_comment_id")
    UUID projectCommentId;

    @Column("project_id")
    UUID projectId;

    @Column("profile_id")
    UUID profileId;

    @Column("comment")
    String comment;

    public ProjectComment(UUID projectCommentId, UUID projectId, UUID profileId, String comment) {
        this.projectCommentId = projectCommentId;
        this.projectId = projectId;
        this.profileId = profileId;
        this.comment = comment;
    }

}