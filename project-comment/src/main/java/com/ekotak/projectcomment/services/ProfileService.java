package com.ekotak.projectcomment.services;

import com.ekotak.projectcomment.domain.Profile;

public interface ProfileService {

    Profile get();

}