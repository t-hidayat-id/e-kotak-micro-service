package com.ekotak.projectcomment;

import static org.assertj.core.api.Assertions.*;

import java.util.List;
import java.util.UUID;

import com.ekotak.projectcomment.domain.ProjectComment;
import com.ekotak.projectcomment.repositories.ProjectCommentRepository;
import com.fasterxml.uuid.Generators;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProjectCommentApplication.class)
public class ProjectCommentRepositoryTests {

    @Autowired
    ProjectCommentRepository projectCommentRepository;
    UUID projectCommentId;

    @Before
    public void setUp() {
        projectCommentId = Generators.timeBasedGenerator().generate();
        
        ProjectComment projectComment = new ProjectComment(
            projectCommentId, 
            Generators.timeBasedGenerator().generate(), 
            Generators.timeBasedGenerator().generate(),
            "Test"
        );
    
        projectCommentRepository.save(projectComment);
    }

    @Test
    public void findByProjectCommentId() {
        ProjectComment projectComment = projectCommentRepository.findByProjectCommentId(
            projectCommentId
        ).orElse(null);

        assertThat(projectComment.getProjectCommentId().equals(projectCommentId)).isTrue();
    }

    @Test
    public void findAll() {
        List<ProjectComment> projectComments = projectCommentRepository.findAll();

        assertThat(projectComments.isEmpty()).isFalse();
    }

    @After
    public void setDone() {
        ProjectComment projectComment = projectCommentRepository.findByProjectCommentId(
            projectCommentId
        ).orElse(null);

        projectCommentRepository.delete(projectComment);
    }

}