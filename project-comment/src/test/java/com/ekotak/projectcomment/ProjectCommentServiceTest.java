package com.ekotak.projectcomment;

import static org.assertj.core.api.Assertions.*;

import java.util.UUID;

import com.ekotak.projectcomment.domain.ProjectComment;
import com.ekotak.projectcomment.services.ProjectCommentService;
import com.fasterxml.uuid.Generators;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProjectCommentApplication.class)
public class ProjectCommentServiceTest {

    @Autowired
    ProjectCommentService projectCommentService;
    UUID projectCommentId;

    @Test
    public void createDeleteFind() {
       ProjectComment projectComment = new ProjectComment(
           null, 
           Generators.timeBasedGenerator().generate(),
           Generators.timeBasedGenerator().generate(),
           "Test"
        );
        projectComment = projectCommentService.create(projectComment);

        projectCommentId = projectComment.getProjectCommentId();
        assertThat(projectCommentId.toString().isEmpty()).isFalse();

        projectCommentService.delete(projectComment);

        projectComment = projectCommentService.findByProjectCommentId(projectCommentId);
        assertThat(projectComment).isNull();
    }

}