package com.ekotak.projectcover.controllers;

import java.util.UUID;

import com.datastax.driver.core.utils.Bytes;
import com.ekotak.projectcover.domain.ProjectCoverOriginal;
import com.ekotak.projectcover.models.ProjectCoverOriginalReponseModel;
import com.ekotak.projectcover.services.ProjectCoverOriginalService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/projects")
public class ProjectCoverOriginalController {

    @Autowired
    ProjectCoverOriginalService projectCoverOriginalService;

    @PostMapping(value = "/{projectId}/original")
    public ResponseEntity<Object> save(
        @PathVariable("projectId") UUID projectId,
        @RequestParam("file") MultipartFile file 
    ) {        
        ProjectCoverOriginal projectCoverOriginal = projectCoverOriginalService.save(projectId, file);
        
        ProjectCoverOriginalReponseModel projectCoverOriginalResponseModel = 
            new ProjectCoverOriginalReponseModel(projectCoverOriginal.getProjectId());
    
        return ResponseEntity.ok().body(projectCoverOriginalResponseModel);
    }

    @GetMapping(value = "/{projectId}/original")
    public ResponseEntity<Object> findByProjectId(
        @PathVariable("projectId") UUID projectId
    ) {
        ProjectCoverOriginal projectCoverOriginal = projectCoverOriginalService.findByProjectId(
            projectId
        );    
        byte image[] = new byte[projectCoverOriginal.getLength()];
        image = Bytes.getArray(projectCoverOriginal.getImage());

        return ResponseEntity.ok()
            .contentType(MediaType.parseMediaType("image/jpeg"))
            .header(
                HttpHeaders.CONTENT_DISPOSITION, 
                "attachment; filename=\"project-cover-original.jpeg\""
            ).body(image);
    }

}