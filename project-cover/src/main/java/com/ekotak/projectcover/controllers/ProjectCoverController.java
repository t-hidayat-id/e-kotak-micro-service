package com.ekotak.projectcover.controllers;

import java.util.UUID;

import javax.validation.Valid;

import com.datastax.driver.core.utils.Bytes;
import com.ekotak.projectcover.domain.ProjectCover;
import com.ekotak.projectcover.models.ProjectCoverGetModel;
import com.ekotak.projectcover.models.ProjectCoverModel;
import com.ekotak.projectcover.services.ProjectCoverService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/projects")
public class ProjectCoverController {

    @Autowired
    ProjectCoverService projectCoverService;

    @PostMapping(value = "/{projectId}/cover")
    public ResponseEntity<Object> save(
        @PathVariable("projectId") UUID projectId,
        @Valid @RequestBody ProjectCoverModel projectCovertModel
    ) {        
        ProjectCover projectCover = projectCoverService.save(projectId, projectCovertModel);
        
        ProjectCoverGetModel projectCoverGetModel = new ProjectCoverGetModel(
            projectCover.getProjectId(), projectCover.getProperties()
        );
        return ResponseEntity.ok().body(projectCoverGetModel);
    }

    @GetMapping(value = "/{projectId}/cover")
    public ResponseEntity<Object> findByProjectId(
        @PathVariable("projectId") UUID projectId
    ) {
        ProjectCover projectCover = projectCoverService.findByProjectId(
            projectId
        );    
        byte image[] = new byte[projectCover.getLength()];
        image = Bytes.getArray(projectCover.getImage());

        return ResponseEntity.ok()
            .contentType(MediaType.parseMediaType("image/jpeg"))
            .header(
                HttpHeaders.CONTENT_DISPOSITION, 
                "attachment; filename=\"project-cover.jpeg\""
            ).body(image);
    }

    @GetMapping(value = "/{projectId}/cover/properties")
    public ResponseEntity<Object> findPropertiesByProjectId(
        @PathVariable("projectId") UUID projectId
    ) {
        ProjectCover projectCover = projectCoverService.findByProjectId(
            projectId
        );    

        ProjectCoverGetModel projectCoverGetModel = new ProjectCoverGetModel(
            projectCover.getProjectId(), projectCover.getProperties()
        );
        return ResponseEntity.ok().body(projectCoverGetModel);
    }

}