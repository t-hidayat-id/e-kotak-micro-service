package com.ekotak.projectcover.security;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ekotak.projectcover.services.UserService;

import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication
    .UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www
    .BasicAuthenticationFilter;

import io.jsonwebtoken.Jwts;

public class AuthorizationFilter extends BasicAuthenticationFilter {
    
	Environment environment;
    UserService userService;

    public AuthorizationFilter(
        AuthenticationManager authManager, 
        Environment environment,
        UserService userService
    ) {
        super(authManager);
        this.environment = environment;
        this.userService = userService;
    }
    
    @Override
    protected void doFilterInternal(HttpServletRequest req,
            HttpServletResponse res,
            FilterChain chain) throws IOException, ServletException {

        String authorizationHeader = req.getHeader(
            environment.getProperty("authorization.token.header.name")
        );

        if (authorizationHeader == null || !authorizationHeader.startsWith(
            environment.getProperty("authorization.token.header.prefix"))) 
        {
            chain.doFilter(req, res);
            return;
        }

        this.userService.setToken(authorizationHeader.split("\\s+")[1]);

        UsernamePasswordAuthenticationToken authentication 
            = getAuthentication(req);
       
        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(req, res);
    }  
    
    private UsernamePasswordAuthenticationToken getAuthentication(
        HttpServletRequest req) 
    {
        String authorizationHeader = req.getHeader(
        environment.getProperty("authorization.token.header.name")
        );
   
        if (authorizationHeader == null) {
            return null;
        }

        String token = authorizationHeader.replace(environment.getProperty(
            "authorization.token.header.prefix"
        ), "");

        String id = Jwts.parser()
            .setSigningKey(environment.getProperty("token.secret"))
            .parseClaimsJws(token)
            .getBody()
            .getSubject();

        if (id == null) {
            return null;
        }
   
        return new UsernamePasswordAuthenticationToken(
            id, null, new ArrayList<>()
        );
     }
     
}
