package com.ekotak.projectcover.domain;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.UUID;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import lombok.Data;

@Data
@Table("project_cover_original")
public class ProjectCoverOriginal implements Serializable {

    private static final long serialVersionUID = 1307775954669676525L;

    @PrimaryKey("project_id")
    private UUID projectId;

    @Column("image")
    ByteBuffer image;

    @Column("length")
    int length;

    @Column("filename")
    String filename;

    public ProjectCoverOriginal() {};

    public ProjectCoverOriginal(UUID projectId, ByteBuffer image, int length, String filename) {
        this.projectId = projectId;
        this.image = image;
        this.length = length;
        this.filename = filename;
    }

}