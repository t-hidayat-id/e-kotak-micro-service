package com.ekotak.projectcover.domain;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.UUID;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import lombok.Data;

@Data
@Table("project_cover")
public class ProjectCover implements Serializable {

    private static final long serialVersionUID = 1L;

    @PrimaryKey("project_id")
    private UUID projectId;

    @Column("image")
    ByteBuffer image;

    @Column("length")
    int length;

    @Column("properties")
    String properties;

    public ProjectCover() {};

    public ProjectCover(UUID projectId, ByteBuffer image, int length, String properties) {
        this.projectId = projectId;
        this.image = image;
        this.length = length;
        this.properties = properties;
    }

}