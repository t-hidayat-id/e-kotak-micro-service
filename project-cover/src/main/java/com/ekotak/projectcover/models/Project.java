package com.ekotak.projectcover.models;

import java.util.UUID;

import lombok.Data;

@Data
public class Project {

    UUID projectId;
    UUID profileId;
    String slug;
    String title;
    String description;

}