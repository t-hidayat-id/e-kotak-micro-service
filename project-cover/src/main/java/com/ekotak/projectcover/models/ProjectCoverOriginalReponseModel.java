package com.ekotak.projectcover.models;

import java.util.UUID;

import lombok.Data;

@Data
public class ProjectCoverOriginalReponseModel {

    UUID projectId;

    public ProjectCoverOriginalReponseModel(UUID projectId) {
        this.projectId = projectId;
    }
}