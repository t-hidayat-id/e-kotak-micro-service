package com.ekotak.projectcover.models;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class ProjectCoverModel {
 
    @NotNull(message = "Properties cannot be null!")
    private String properties;

    @NotNull(message = "Encoded image cannot be null!")
    private String encodedImage;    

}