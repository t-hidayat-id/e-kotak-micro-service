package com.ekotak.projectcover.models;

import java.util.UUID;

import lombok.Data;

@Data
public class ProjectCoverReponseModel {

    UUID projectId;

    public ProjectCoverReponseModel(UUID projectId) {
        this.projectId = projectId;
    }
}