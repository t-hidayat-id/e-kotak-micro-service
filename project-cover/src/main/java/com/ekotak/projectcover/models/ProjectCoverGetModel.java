package com.ekotak.projectcover.models;

import java.util.UUID;

import lombok.Data;

@Data
public class ProjectCoverGetModel {

    UUID projectId;
    String properties;

    public ProjectCoverGetModel(UUID projectId, String properties) {
        this.projectId = projectId;
        this.properties = properties;
    }
}