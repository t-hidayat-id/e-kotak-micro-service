package com.ekotak.projectcover.services;

import java.util.UUID;

import com.ekotak.projectcover.domain.ProjectCover;
import com.ekotak.projectcover.models.ProjectCoverModel;

public interface ProjectCoverService {

    ProjectCover save(UUID projectId, ProjectCoverModel projectCoverModel);
    ProjectCover findByProjectId(UUID projectId);

}