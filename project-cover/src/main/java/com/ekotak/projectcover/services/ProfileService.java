package com.ekotak.projectcover.services;

import com.ekotak.projectcover.models.Profile;

public interface ProfileService {

    Profile get();

}