package com.ekotak.projectcover.services;

import com.ekotak.projectcover.exceptions.ProfileNotFoundException;
import com.ekotak.projectcover.models.Profile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProfileServiceImpl implements ProfileService {

    UserService userService;
    ProfileServiceClient profileServiceClient;

    private Profile profile;
    
    @Autowired
    public ProfileServiceImpl(
        UserService userService, 
        ProfileServiceClient profileServiceClient
    ) {
        this.userService = userService;
        this.profileServiceClient = profileServiceClient;
    }

    public Profile get() {
        if (profile == null) {
            String userId = this.userService.getId();
            profile = this.profileServiceClient.findByUserId(userId);
            
            if (profile == null) 
                throw new ProfileNotFoundException(userId);
        }
        
        return profile;
    }
    
}