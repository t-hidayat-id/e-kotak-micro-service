package com.ekotak.projectcover.services;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.UUID;

import com.ekotak.projectcover.domain.ProjectCoverOriginal;
import com.ekotak.projectcover.exceptions.ProfileNotOwnerProjectException;
import com.ekotak.projectcover.exceptions.ProjectCoverOriginalNotFoundException;
import com.ekotak.projectcover.exceptions.ProjectCoverOriginalNotSavedException;
import com.ekotak.projectcover.exceptions.ProjectNotFoundException;
import com.ekotak.projectcover.models.Profile;
import com.ekotak.projectcover.models.Project;
import com.ekotak.projectcover.repositories.ProjectCoverOriginalRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class ProjectCoverOrignalServiceImpl implements ProjectCoverOriginalService {

    ProfileService profileService;
    ProjectServiceClient projectServiceClient;
    ProjectCoverOriginalRepository projectCoverOriginalRepository;

    @Autowired
    public ProjectCoverOrignalServiceImpl(
        ProfileService profileService,
        ProjectCoverOriginalRepository projectCoverOriginalRepository,
        ProjectServiceClient projectServiceClient
    ) {
        this.profileService = profileService;
        this.projectCoverOriginalRepository = projectCoverOriginalRepository;
        this.projectServiceClient = projectServiceClient;
    }

    @Override
    public ProjectCoverOriginal save(UUID projectId, MultipartFile file) {
        Project project = projectServiceClient.findByProjectId(
            projectId.toString()
        );
        if (project == null) {
            throw new ProjectNotFoundException(projectId);
        }
        
        Profile profile = profileService.get();
        if (!profile.getProfileId().equals(project.getProfileId())) {
            throw new ProfileNotOwnerProjectException(
                profile.getUserId().toString(),
                profile.getProfileId().toString(),
                project.getProjectId().toString()
            );
        }

        try {
            InputStream inputStream = new BufferedInputStream(
                file.getInputStream()
            );
            byte[] bytes = new byte[inputStream.available() + 1];
            inputStream.read(bytes);

            ByteBuffer byteBuffer = ByteBuffer.wrap(bytes);
            ProjectCoverOriginal projectCoverOriginal = new ProjectCoverOriginal(
                projectId, byteBuffer, bytes.length,  file.getOriginalFilename()
            );

            return projectCoverOriginalRepository.save(projectCoverOriginal);
        } catch (IOException ioException) {
            throw new ProjectCoverOriginalNotSavedException();
        }
    }

    @Override
    public ProjectCoverOriginal findByProjectId(UUID projectId) {
        return this.projectCoverOriginalRepository.findByProjectId(
            projectId
        ).orElseThrow(() -> new ProjectCoverOriginalNotFoundException(projectId));
    }
    
}