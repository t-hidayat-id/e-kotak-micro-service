package com.ekotak.projectcover.services;

import java.util.UUID;

import com.ekotak.projectcover.domain.ProjectCoverOriginal;

import org.springframework.web.multipart.MultipartFile;

public interface ProjectCoverOriginalService {

    ProjectCoverOriginal save(UUID projectId, MultipartFile file);
    ProjectCoverOriginal findByProjectId(UUID projectId);

}