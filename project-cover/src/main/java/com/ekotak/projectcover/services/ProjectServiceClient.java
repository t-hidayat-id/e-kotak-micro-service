package com.ekotak.projectcover.services;

import com.ekotak.projectcover.models.Project;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name="project-mc")
public interface ProjectServiceClient {

    @GetMapping("/projects/{projectId}")
    public Project findByProjectId(@PathVariable String projectId);

}