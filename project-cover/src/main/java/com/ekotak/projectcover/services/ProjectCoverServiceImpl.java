package com.ekotak.projectcover.services;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.UUID;

import com.ekotak.projectcover.domain.ProjectCover;
import com.ekotak.projectcover.exceptions.ProfileNotOwnerProjectException;
import com.ekotak.projectcover.exceptions.ProjectCoverNotFoundException;
import com.ekotak.projectcover.exceptions.ProjectCoverNotSavedException;
import com.ekotak.projectcover.exceptions.ProjectNotFoundException;
import com.ekotak.projectcover.models.Profile;
import com.ekotak.projectcover.models.Project;
import com.ekotak.projectcover.models.ProjectCoverModel;
import com.ekotak.projectcover.repositories.ProjectCoverRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectCoverServiceImpl implements ProjectCoverService {

    ProfileService profileService;
    ProjectCoverRepository projectCoverRepository;
    ProjectServiceClient projectServiceClient;


    @Autowired
    public ProjectCoverServiceImpl(
        ProfileService profileService,
        ProjectCoverRepository projectCoverRepository,
        ProjectServiceClient projectServiceClient
    ) {
        this.profileService = profileService;
        this.projectCoverRepository = projectCoverRepository;
        this.projectServiceClient = projectServiceClient;
    }

    @Override
    public ProjectCover save(UUID projectId, ProjectCoverModel projectCoverModel) {
        Project project = projectServiceClient.findByProjectId(
            projectId.toString()
        );
        if (project == null) {
            throw new ProjectNotFoundException(projectId);
        }
        
        Profile profile = profileService.get();
        if (!profile.getProfileId().equals(project.getProfileId())) {
            throw new ProfileNotOwnerProjectException(
                profile.getUserId().toString(),
                profile.getProfileId().toString(),
                project.getProjectId().toString()
            );
        }

        try {
            String delimiterEncoded = "[,]";
            String[] parts = projectCoverModel.getEncodedImage().split(delimiterEncoded);

            byte[] imageByte = Base64.getDecoder().decode(parts[1].getBytes(StandardCharsets.UTF_8));
            InputStream inputStream = new ByteArrayInputStream(imageByte);
            
            byte[] bytes = new byte[inputStream.available() + 1];
            inputStream.read(bytes);
            ByteBuffer byteBuffer = ByteBuffer.wrap(bytes);
            
            ProjectCover projectCover = new ProjectCover(
                projectId,  byteBuffer,  bytes.length, projectCoverModel.getProperties()
            );

            return projectCoverRepository.save(projectCover);
        } catch (IOException ioException) {
            throw new ProjectCoverNotSavedException();
        }
    }

    @Override
    public ProjectCover findByProjectId(UUID projectId) {
        return this.projectCoverRepository.findByProjectId(
            projectId
        ).orElseThrow(() -> new ProjectCoverNotFoundException(projectId));
    }
    
}