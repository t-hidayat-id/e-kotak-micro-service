package com.ekotak.projectcover.exceptions;

public class ProfileNotOwnerProjectException extends RuntimeException {

    private static final long serialVersionUID = 1530208408343578765L;

    private String userId;
    private String profileId;
    private String projectId;

    public ProfileNotOwnerProjectException() {}

    public ProfileNotOwnerProjectException(
        String userId,
        String profileId,
        String projectId
    ) {
        super(
            "Only the owner of the project can create" +
            " or update the project cover."
        );
        
        this.userId = userId;
        this.profileId = profileId;
        this.projectId = projectId;
    }

    public String getUserId() {
        return this.userId;
    }

    public String getProfileId() {
        return this.profileId;
    }

    public String getProjectId() {
        return this.projectId;
    }

}