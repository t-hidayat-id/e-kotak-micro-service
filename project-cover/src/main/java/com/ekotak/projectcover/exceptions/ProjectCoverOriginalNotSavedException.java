package com.ekotak.projectcover.exceptions;

import java.util.UUID;

public class ProjectCoverOriginalNotSavedException extends RuntimeException {

    private static final long serialVersionUID = 3822519076153818269L;
    private UUID projectId;

    public ProjectCoverOriginalNotSavedException() {}

    public ProjectCoverOriginalNotSavedException(UUID projectId) {
        super("Could not save project cover original. Please try again!");
        this.projectId = projectId;
    }

    public UUID getProjectId() {
        return this.projectId;
    }

}