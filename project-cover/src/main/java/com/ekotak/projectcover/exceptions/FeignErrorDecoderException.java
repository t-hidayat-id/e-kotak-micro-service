package com.ekotak.projectcover.exceptions;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.UUID;

import org.springframework.stereotype.Component;

import feign.Response;
import feign.codec.ErrorDecoder;

@Component
public class FeignErrorDecoderException implements ErrorDecoder {

    @Override
    public Exception decode(String methodKey, Response response) {
        switch (response.status()) {
        case 404:
            if (methodKey.contains(
                "ProjectServiceClient#findByProjectId(String)"
            )) {
                UUID projectId = null;
                try {
                    URI uri = new URI(response.request().url().toString());
                    String path = uri.getPath();
                    projectId = UUID.fromString(
                        path.substring(path.lastIndexOf('/') + 1)
                    );
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
                return new ProjectNotFoundException(projectId);
            }
            break;
        }
        return new Exception(response.reason());
    }
}