package com.ekotak.projectcover.exceptions;

import java.util.UUID;

public class ProjectCoverNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 9046693557485355854L;

    private UUID projectId;

    public ProjectCoverNotFoundException() {

    }

    public ProjectCoverNotFoundException(UUID projectId) {
        super("Project cover is not found.");
        
        this.projectId = projectId;
    }

    public UUID getProjectId() {
        return this.projectId;
    }

}