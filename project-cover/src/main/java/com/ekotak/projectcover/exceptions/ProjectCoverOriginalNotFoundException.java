package com.ekotak.projectcover.exceptions;

import java.util.UUID;

public class ProjectCoverOriginalNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -5921320270975956311L;
    private UUID projectId;

    public ProjectCoverOriginalNotFoundException() { }

    public ProjectCoverOriginalNotFoundException(UUID projectId) {
        super("Project cover oirginal is not found.");
        
        this.projectId = projectId;
    }

    public UUID getProjectId() {
        return this.projectId;
    }

}