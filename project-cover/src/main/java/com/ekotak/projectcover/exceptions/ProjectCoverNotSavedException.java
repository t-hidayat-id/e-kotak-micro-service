package com.ekotak.projectcover.exceptions;

import java.util.UUID;

public class ProjectCoverNotSavedException extends RuntimeException {

    private static final long serialVersionUID = 9046693557485355854L;

    private UUID projectId;

    public ProjectCoverNotSavedException() {}

    public ProjectCoverNotSavedException(UUID projectId) {
        super("Could not save project cover. Please try again!");
        this.projectId = projectId;
    }

    public UUID getProjectId() {
        return this.projectId;
    }

}