package com.ekotak.projectcover.repositories;

import java.util.Optional;
import java.util.UUID;

import com.ekotak.projectcover.domain.ProjectCover;

import org.springframework.data.repository.CrudRepository;

public interface ProjectCoverRepository 
    extends CrudRepository<ProjectCover, UUID> 
{
   
    Optional<ProjectCover> findByProjectId(UUID projectId);

}