package com.ekotak.projectcover.repositories;

import java.util.Optional;
import java.util.UUID;

import com.ekotak.projectcover.domain.ProjectCoverOriginal;

import org.springframework.data.repository.CrudRepository;

public interface ProjectCoverOriginalRepository 
    extends CrudRepository<ProjectCoverOriginal, UUID> 
{
   
    Optional<ProjectCoverOriginal> findByProjectId(UUID projectId);

}