package com.ekotak.category.services;

import java.util.List;
import java.util.UUID;

import com.ekotak.category.domain.Category;
import com.ekotak.category.repositories.CategoryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImpl implements CategoryService {

    CategoryRepository categoryRepository; 

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Category findByCategoryId(UUID categoryId) {
        Category projectCategory = categoryRepository.findByCategoryId(
            categoryId
        ).orElse(null);

        return projectCategory;
    }
    
}