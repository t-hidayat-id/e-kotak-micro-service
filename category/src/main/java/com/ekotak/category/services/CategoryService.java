package com.ekotak.category.services;

import java.util.List;
import java.util.UUID;

import com.ekotak.category.domain.Category;

public interface CategoryService {

    Category findByCategoryId(UUID categoryId);
    List<Category> findAll();

}