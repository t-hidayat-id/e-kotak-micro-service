package com.ekotak.category.domain;

import java.io.Serializable;
import java.util.UUID;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import lombok.Data;

@Data
@Table("category")
public class Category implements Serializable {

    private static final long serialVersionUID = 1L;

    @PrimaryKey("category_id")
    UUID categoryId;

    @Column("name")
    String name;

    public Category() {
    }

}