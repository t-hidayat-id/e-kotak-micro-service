package com.ekotak.category.controllers;

import java.util.UUID;

import com.ekotak.category.services.CategoryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/categories")
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    @GetMapping()
    public ResponseEntity<Object> findAll() {
        return ResponseEntity.status(HttpStatus.OK).body(
            categoryService.findAll()
        );
    }

    @GetMapping(value="/{categoryId}")
    public ResponseEntity<Object> findByCategoryId(@PathVariable("categoryId") UUID categoryId)
    {
        return ResponseEntity.status(HttpStatus.OK).body(
            categoryService.findByCategoryId(categoryId)
        );
    }
    
}