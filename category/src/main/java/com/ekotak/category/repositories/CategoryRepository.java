package com.ekotak.category.repositories;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.ekotak.category.domain.Category;

import org.springframework.data.repository.CrudRepository;

public interface CategoryRepository extends  CrudRepository<Category,  UUID> {

    Optional<Category> findByCategoryId(UUID categoryId);
    List<Category> findAll(); 

}