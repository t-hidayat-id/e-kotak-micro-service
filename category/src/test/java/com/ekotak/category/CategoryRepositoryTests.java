package com.ekotak.category;

import static org.assertj.core.api.Assertions.*;

import java.util.List;
import java.util.UUID;

import com.ekotak.category.domain.Category;
import com.ekotak.category.repositories.CategoryRepository;
import com.fasterxml.uuid.Generators;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CategoryApplication.class)
public class CategoryRepositoryTests {

    @Autowired
    CategoryRepository categoryRepository;
    UUID categoryId;

    @Before
    public void setUp() {
        categoryId = Generators.timeBasedGenerator().generate();
        Category category = new Category();
        category.setName("Testing");
        category.setCategoryId(categoryId);

        categoryRepository.save(category);
    }

    @Test
    public void findAll() {
        List<Category> categories = categoryRepository.findAll();

        assertThat(categories.isEmpty()).isFalse();
    }

    @Test
    public void findByProjectCategoryId() {
        Category category = categoryRepository.findByCategoryId(
            categoryId
        ).orElse(null);

        assertThat(category.getCategoryId().equals(categoryId)).isTrue();
    }

    @After
    public void setDone() {
        Category category = categoryRepository.findByCategoryId(
            categoryId
        ).orElse(null);
        
        categoryRepository.delete(category);
    }

}