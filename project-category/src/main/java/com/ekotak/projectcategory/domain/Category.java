package com.ekotak.projectcategory.domain;

import java.io.Serializable;
import java.util.UUID;

import lombok.Data;

@Data
public class Category implements Serializable {

    private static final long serialVersionUID = -1071822365918092535L;

    UUID categoryId;
    String name;

    public Category() {}

    public Category(UUID categoryId, String name) {
        this.categoryId = categoryId;
        this.name = name;
    }

}