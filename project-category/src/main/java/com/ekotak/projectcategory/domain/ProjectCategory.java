package com.ekotak.projectcategory.domain;

import java.io.Serializable;
import java.util.UUID;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import lombok.Data;

@Data
@Table("project_category")
public class ProjectCategory implements Serializable {

    private static final long serialVersionUID = -6055299955950900198L;

    @PrimaryKey("project_category_id")
    UUID projectCategoryId;

    @Column("project_id")
    UUID projectId;

    @Column("category_id")
    UUID categoryId;

    public ProjectCategory() {};

    public ProjectCategory(UUID projectCategoryId, UUID projectId, UUID categoryId) {
        this.projectCategoryId = projectCategoryId;
        this.projectId = projectId;
        this.categoryId = categoryId;
    }

}