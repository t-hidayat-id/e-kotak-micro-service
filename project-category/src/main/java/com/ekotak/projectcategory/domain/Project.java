package com.ekotak.projectcategory.domain;

import java.io.Serializable;
import java.util.UUID;

import lombok.Data;

@Data
public class Project implements Serializable {

    private static final long serialVersionUID = 7823551093374144890L;

    UUID projectId;
    UUID profileId;
    String slug;
    String title;
    String description;

    public Project() {}

}