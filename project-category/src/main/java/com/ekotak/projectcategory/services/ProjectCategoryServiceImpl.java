package com.ekotak.projectcategory.services;

import java.util.UUID;

import com.ekotak.projectcategory.domain.ProjectCategory;
import com.ekotak.projectcategory.exceptions.ProjectCategoryNotFoundException;
import com.ekotak.projectcategory.repositories.ProjectCategoryRepository;
import com.fasterxml.uuid.Generators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectCategoryServiceImpl implements ProjectCategoryService {

    ProjectCategoryRepository projectCategoryRepository;
    ProjectCategoryProducerService projectCategoryProducerService;

    @Autowired
    public ProjectCategoryServiceImpl(
        ProjectCategoryRepository projectCategoryRepository,
        ProjectCategoryProducerService projectCategoryProducerService
    ) {
        this.projectCategoryRepository = projectCategoryRepository;
        this.projectCategoryProducerService = projectCategoryProducerService;
    }

    @Override
    public ProjectCategory create(ProjectCategory projectCategory) {
        UUID projectCategoryId = Generators.timeBasedGenerator().generate();
        projectCategory.setProjectCategoryId(projectCategoryId);

        projectCategory = projectCategoryRepository.save(projectCategory);

        projectCategoryProducerService.send(projectCategory);

        return projectCategory;
    }

    @Override
    public boolean delete(UUID projectCategoryId) {   
        ProjectCategory projectCategory = this.findByProjectCategoryId(projectCategoryId);     
        projectCategoryRepository.delete(projectCategory);
        
        return true;
    }

    @Override
    public ProjectCategory findByProjectCategoryId(UUID projectCategoryId) {
        ProjectCategory projectCategory = projectCategoryRepository.findByProjectCategoryId(
            projectCategoryId
        ).orElseThrow(() -> new ProjectCategoryNotFoundException(projectCategoryId));

        return projectCategory;
    }

}