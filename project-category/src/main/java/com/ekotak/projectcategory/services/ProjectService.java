package com.ekotak.projectcategory.services;

import java.util.UUID;

import com.ekotak.projectcategory.domain.Project;;

public interface ProjectService {

    Project findByProjectId(UUID projectId);

}