package com.ekotak.projectcategory.services;

import java.util.UUID;

import com.ekotak.projectcategory.domain.Category;

public interface CategoryService {

    Category get(UUID categoryId);
    
}