package com.ekotak.projectcategory.services;

import com.ekotak.projectcategory.domain.ProjectCategory;

import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class ProjectCategoryProducerService {

    private final KafkaTemplate<Object, ProjectCategory> kafkaTemplate;

    ProjectCategoryProducerService(KafkaTemplate<Object, ProjectCategory> kafkaTemplate) {
		this.kafkaTemplate = kafkaTemplate;
	}

	public void send(ProjectCategory projectCategory) {
		this.kafkaTemplate.send("projectCategoryTopic", projectCategory);
	}

}