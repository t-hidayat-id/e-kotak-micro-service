package com.ekotak.projectcategory.services;

import com.ekotak.projectcategory.domain.Profile;

public interface ProfileService {

    Profile get();

}