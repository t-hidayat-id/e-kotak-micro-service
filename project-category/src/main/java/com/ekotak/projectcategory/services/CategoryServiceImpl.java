package com.ekotak.projectcategory.services;

import java.util.UUID;

import com.ekotak.projectcategory.domain.Category;
import com.ekotak.projectcategory.exceptions.NotFoundOrUnavailableException;
import com.netflix.hystrix.exception.HystrixRuntimeException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    Environment environment;
    
    CategoryServiceClient categoryServiceClient;
    Category category;

    @Autowired
    CategoryServiceImpl(CategoryServiceClient categoryServiceClient) {
        this.categoryServiceClient = categoryServiceClient;
    }

    @Override
    public Category get(UUID categoryId) {
        try {
            category = categoryServiceClient.findByCategoryId(categoryId.toString());
        } catch (HystrixRuntimeException ex) {
            throw new NotFoundOrUnavailableException(
                ex.getCause(), 
                categoryId.toString(), 
                environment.getProperty("microservice.category.name"), 
                environment.getProperty("microservice.category.exception.message.not-found")
            );
        }
        return category;
    }
    
}