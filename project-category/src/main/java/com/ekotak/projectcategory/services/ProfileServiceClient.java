package com.ekotak.projectcategory.services;

import com.ekotak.projectcategory.domain.Profile;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Service
@FeignClient(name="profile-mc")
public interface ProfileServiceClient {

    @GetMapping("/profiles/{userId}")
    public Profile findByUserId(@PathVariable String userId);

}

