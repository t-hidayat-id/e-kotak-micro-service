package com.ekotak.projectcategory.services;

import com.ekotak.projectcategory.domain.Category;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Service
@FeignClient(name="category-mc")
public interface CategoryServiceClient {

    @GetMapping("/categories/{categoryId}")
    public Category findByCategoryId(@PathVariable String categoryId);

}
