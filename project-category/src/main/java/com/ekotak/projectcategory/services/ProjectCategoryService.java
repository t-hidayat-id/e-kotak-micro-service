package com.ekotak.projectcategory.services;

import java.util.UUID;

import com.ekotak.projectcategory.domain.ProjectCategory;

public interface ProjectCategoryService {

    ProjectCategory create(ProjectCategory projectCategory);
    boolean delete(UUID projectCategoryId);
    ProjectCategory findByProjectCategoryId(UUID projectCategoryId);

}