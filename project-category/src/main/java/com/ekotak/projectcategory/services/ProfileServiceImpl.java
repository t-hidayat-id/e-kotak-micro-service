package com.ekotak.projectcategory.services;

import com.ekotak.projectcategory.exceptions.MicroServiceDownException;
import com.ekotak.projectcategory.exceptions.ProfileNotFoundException;
import com.netflix.hystrix.exception.HystrixRuntimeException;
import com.ekotak.projectcategory.domain.Profile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import feign.FeignException;

@Service
public class ProfileServiceImpl implements ProfileService {

    UserService userService;
    ProfileServiceClient profileServiceClient;

    private Profile profile;
    
    @Autowired
    public ProfileServiceImpl(
        UserService userService, 
        ProfileServiceClient profileServiceClient
    ) {
        this.userService = userService;
        this.profileServiceClient = profileServiceClient;
    }

    public Profile get() {
        if (profile == null) {
            String userId = this.userService.getId();
            try {
                profile = this.profileServiceClient.findByUserId(userId);
             } catch (HystrixRuntimeException ex) {
                Throwable cause = ex.getCause();
                if (cause instanceof FeignException && ((FeignException) cause).status() == 404) {
                    throw new ProfileNotFoundException(userId);
                } else {
                    throw new MicroServiceDownException(cause.getCause().getMessage(), "profile-mc");
                }
             }
             
            if (profile == null) throw new ProfileNotFoundException(userId);
        }
        
        return profile;
    }
    
}