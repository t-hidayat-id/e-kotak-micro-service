package com.ekotak.projectcategory.services;

import com.ekotak.projectcategory.domain.Project;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name="project-mc")
public interface ProjectServiceClient {

    @GetMapping("/projects/{projectId}")
    public Project findByProjectId(@PathVariable String projectId);

}