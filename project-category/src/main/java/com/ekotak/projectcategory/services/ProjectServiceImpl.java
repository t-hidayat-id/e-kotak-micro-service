package com.ekotak.projectcategory.services;

import java.util.UUID;

import com.ekotak.projectcategory.domain.Project;
import com.ekotak.projectcategory.exceptions.MicroServiceDownException;
import com.ekotak.projectcategory.exceptions.ProjectNotFoundException;
import com.netflix.hystrix.exception.HystrixRuntimeException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import feign.FeignException;

@Service
public class ProjectServiceImpl implements ProjectService {

    ProjectServiceClient projectServiceClient;
    Project project;

    @Autowired
    public ProjectServiceImpl(ProjectServiceClient projectServiceClient) {
        this.projectServiceClient = projectServiceClient;
    }

    public Project findByProjectId(UUID projectId) {
        try {
            project = projectServiceClient.findByProjectId(projectId.toString());
        } catch (HystrixRuntimeException ex) {
            Throwable cause = ex.getCause();
            if (cause instanceof FeignException && ((FeignException) cause).status() == 404) {
                throw new ProjectNotFoundException(projectId);
            }  else {
                throw new MicroServiceDownException(cause.getCause().getMessage(), "project-mc");
            }
        }
        return project;
    }

}