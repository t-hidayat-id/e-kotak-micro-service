package com.ekotak.projectcategory.controllers;

import javax.validation.Valid;

import com.ekotak.projectcategory.domain.ProjectCategory;
import com.ekotak.projectcategory.models.ProjectCategoryPostModel;
import com.ekotak.projectcategory.services.ProjectCategoryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/project-categories")
public class ProjectCategoryController {

    @Autowired
    ProjectCategoryService projectCategoryService;

    @PostMapping(
        consumes = { 
            MediaType.APPLICATION_JSON_VALUE 
        },
		produces = { 
            MediaType.APPLICATION_JSON_VALUE 
        }
	)
    public ResponseEntity<Object> create(
        @Valid @RequestBody ProjectCategoryPostModel projectCategoryPostModel
    ) {
        ProjectCategory projectCategory = new ProjectCategory(
            null, 
            projectCategoryPostModel.getProjectId(), 
            projectCategoryPostModel.getCategoryId());

        return ResponseEntity.status(HttpStatus.CREATED).body(
            projectCategoryService.create(projectCategory)
        );
    }
}