package com.ekotak.projectcategory.exceptions;

import org.springframework.http.HttpStatus;

public class ObjectException extends RuntimeException {

    private static final long serialVersionUID = 1L;

     private String message;
    private String  objectId;
    private String objectName;
    private  HttpStatus  httpStatus;

    public ObjectException() {}

    public ObjectException(String message,  String objectId, HttpStatus httpStatus) {
        super(message);
        
        this.message = message;
        this.objectId = objectId;
        this.httpStatus = httpStatus;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setHttpStatus(HttpStatus  httpStatus) {
        this.httpStatus = httpStatus;
    }

    public HttpStatus getHttpStatus() {
        return  httpStatus;
    }

}