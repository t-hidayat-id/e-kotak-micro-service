package com.ekotak.projectcategory.exceptions;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation
    .ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler 
    extends ResponseEntityExceptionHandler 
{

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
        MethodArgumentNotValidException ex,
        HttpHeaders headers,
        HttpStatus status, WebRequest request) 
    {
		Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", new Date());
        body.put("status", status.name());

        Map<String, String> errors = new LinkedHashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });

        body.put("errors", errors);

        return new ResponseEntity<>(body, headers, status);
    }

    @ExceptionHandler(value = {ProfileNotFoundException.class})
    protected ResponseEntity<Object> profileNotFound(
        ProfileNotFoundException ex, WebRequest request) 
    {
        List<Object> errorList = new ArrayList<Object>();    
        HashMap<String, String> error = new HashMap<String, String>();
            
        error.put("userId", ex.getUserId());
        errorList.add(error);
    
        RestException restException = 
            new RestException(
                HttpStatus.NOT_FOUND, 
                ex.getLocalizedMessage(), 
                errorList
            );

        return handleExceptionInternal(
            ex, 
            restException, 
            new HttpHeaders(), 
            HttpStatus.NOT_FOUND, 
            request
        );
    }

    @ExceptionHandler(value = {ProjectNotFoundException.class})
    protected ResponseEntity<Object> projectNotFound(
        ProjectNotFoundException ex, WebRequest request) 
    {
        List<Object> errorList = new ArrayList<Object>();    
        HashMap<String, String> error = new HashMap<String, String>();
            
        error.put("projectId", ex.getProjectId().toString());
        errorList.add(error);
    
        RestException restException = 
            new RestException(
                HttpStatus.NOT_FOUND, 
                ex.getLocalizedMessage(), 
                errorList
            );

        return handleExceptionInternal(
            ex, 
            restException, 
            new HttpHeaders(), 
            HttpStatus.NOT_FOUND, 
            request
        );
    }

    @ExceptionHandler(value = {ProjectCategoryNotFoundException.class})
    protected ResponseEntity<Object> projectCategoryNotFound(
        ProjectCategoryNotFoundException ex, WebRequest request) 
    {
        List<Object> errorList = new ArrayList<Object>();    
        HashMap<String, String> error = new HashMap<String, String>();
            
        error.put("projectCategoryId", ex.getProjectCategoryId().toString());
        errorList.add(error);
    
        RestException restException = 
            new RestException(
                HttpStatus.NOT_FOUND, 
                ex.getLocalizedMessage(), 
                errorList
            );

        return handleExceptionInternal(
            ex, 
            restException, 
            new HttpHeaders(), 
            HttpStatus.NOT_FOUND, 
            request
        );
    }

    @ExceptionHandler(value = {MicroServiceDownException.class})
    protected ResponseEntity<Object> microServiceDown(
        MicroServiceDownException ex, WebRequest request) 
    {
        List<Object> errorList = new ArrayList<Object>();    
        HashMap<String, String> error = new HashMap<String, String>();
            
        error.put("service", ex.getService());
        errorList.add(error);
    
        RestException restException = 
            new RestException(
                HttpStatus.SERVICE_UNAVAILABLE, 
                ex.getLocalizedMessage(), 
                errorList
            );

        return handleExceptionInternal(
            ex, 
            restException, 
            new HttpHeaders(), 
            HttpStatus.SERVICE_UNAVAILABLE, 
            request
        );
    }

    @ExceptionHandler(value = {ObjectException.class})
    protected ResponseEntity<Object> object(
        ObjectException ex, WebRequest request) 
    {
        List<Object> errorList = new ArrayList<Object>();    
        HashMap<String, String> error = new HashMap<String, String>();
            
        error.put(ex.getObjectName(), ex.getObjectId());
        errorList.add(error);
    
        RestException restException = 
            new RestException(
                ex.getHttpStatus(), 
                ex.getMessage(), 
                errorList
            );

        return handleExceptionInternal(
            ex, 
            restException, 
            new HttpHeaders(), 
            ex.getHttpStatus(), 
            request
        );
    }

}