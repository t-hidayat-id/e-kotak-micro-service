package com.ekotak.projectcategory.exceptions;

import java.util.UUID;

public class ProjectCategoryNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 9046693557485355854L;

    private UUID projecCategoryId;

    public ProjectCategoryNotFoundException() {}

    public ProjectCategoryNotFoundException(UUID projecCategoryId) {
        super("Project is not found!");
        
        this.projecCategoryId = projecCategoryId;
    }

    public UUID getProjectCategoryId() {
        return this.projecCategoryId;
    }

}