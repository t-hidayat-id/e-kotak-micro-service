package com.ekotak.projectcategory.exceptions;

public class MicroServiceDownException extends RuntimeException {

    private static final long serialVersionUID = 3154394285601414270L;
    
    private String  service;

    public MicroServiceDownException() {}

    public MicroServiceDownException(String message, String service) {
        super(message);
        
        this.service = service;
    }

    public String getService() {
        return service;
    }

}