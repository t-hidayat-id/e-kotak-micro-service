package com.ekotak.projectcategory.exceptions;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpStatus;

import lombok.Data;

@Data
public class RestException {

    private LocalDateTime timestamp;
    private HttpStatus status;
    private String message;
    private List<Object> errors;

    public RestException(
        HttpStatus status, 
        String message, 
        List<Object> errors
    ) {
        super();

        this.timestamp = LocalDateTime.now();
        this.status = status;
        this.message = message;
        this.errors = errors;
    }
 
    public RestException(HttpStatus status, String message, Object error) {
        super();

        this.timestamp = LocalDateTime.now();
        this.status = status;
        this.message = message;
        this.errors = Arrays.asList(error);
    }

}