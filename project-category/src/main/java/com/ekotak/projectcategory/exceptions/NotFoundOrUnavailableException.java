package com.ekotak.projectcategory.exceptions;

import org.springframework.http.HttpStatus;

import feign.FeignException;

public class NotFoundOrUnavailableException extends  ObjectException {

    private static final long serialVersionUID = -1949568544875710767L;

    public NotFoundOrUnavailableException(
        Throwable cause, String  objectId, String serviceName, String message
    ) {
        if (cause instanceof FeignException && ((FeignException) cause).status() == 404) {
            this.setMessage(message);
            this.setObjectId(objectId.toString());
            this.setHttpStatus(HttpStatus.NOT_FOUND);
        } else {
            this.setMessage(cause.getCause().getMessage());
            this.setObjectId(serviceName);
            this.setHttpStatus(HttpStatus.SERVICE_UNAVAILABLE);   
        }
    }
    
}