package com.ekotak.projectcategory.models;

import java.util.UUID;

import javax.validation.constraints.NotNull;

import com.ekotak.projectcategory.validators.IsProjectOwner;

import lombok.Data;

@Data
public class ProjectCategoryPostModel {

    @NotNull(message = "Project Id cannot be null.")
    @IsProjectOwner
    UUID projectId;

    @NotNull(message = "Category Id cannot be null.")
    UUID categoryId;

}