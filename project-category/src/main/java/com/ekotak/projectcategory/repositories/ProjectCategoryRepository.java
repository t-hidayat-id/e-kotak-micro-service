package com.ekotak.projectcategory.repositories;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.ekotak.projectcategory.domain.ProjectCategory;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectCategoryRepository extends CrudRepository<ProjectCategory, UUID> {
   
    Optional<ProjectCategory> findByProjectCategoryId(UUID projectCategoryId);
    List<ProjectCategory> findAll(); 

}