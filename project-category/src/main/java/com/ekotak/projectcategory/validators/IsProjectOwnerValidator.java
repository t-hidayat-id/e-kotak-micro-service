package com.ekotak.projectcategory.validators;

import java.util.UUID;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.ekotak.projectcategory.domain.Profile;
import com.ekotak.projectcategory.domain.Project;
import com.ekotak.projectcategory.services.ProfileService;
import com.ekotak.projectcategory.services.ProjectService;

import org.springframework.beans.factory.annotation.Autowired;

public class IsProjectOwnerValidator 
    implements ConstraintValidator<IsProjectOwner, UUID> 
{

    @Autowired
    ProfileService profileService;

    @Autowired
    ProjectService projectService;
    
    @Override
    public boolean isValid(
        UUID projectId, ConstraintValidatorContext context
    ) {

        Profile profile = profileService.get();
        Project project = projectService.findByProjectId(projectId);

        if (!project.getProfileId().equals(profile.getProfileId())) {
            context.buildConstraintViolationWithTemplate(
                "Only the owner can update this project!"
            ).addConstraintViolation();

            return false;
        }

        return true;
    }

}
