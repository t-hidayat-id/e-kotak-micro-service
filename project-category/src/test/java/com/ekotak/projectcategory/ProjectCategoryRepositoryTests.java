package com.ekotak.projectcategory;

import static org.assertj.core.api.Assertions.*;

import java.util.List;
import java.util.UUID;

import com.ekotak.projectcategory.domain.ProjectCategory;
import com.ekotak.projectcategory.repositories.ProjectCategoryRepository;
import com.fasterxml.uuid.Generators;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProjectCategoryApplication.class)
public class ProjectCategoryRepositoryTests {

    @Autowired
    ProjectCategoryRepository projectCategoryRepository;
    UUID projectCategoryId;

    @Before
    public void setUp() {
        projectCategoryId = Generators.timeBasedGenerator().generate();
        
        ProjectCategory projectCategory = new ProjectCategory(
            projectCategoryId, 
            Generators.timeBasedGenerator().generate(), 
            Generators.timeBasedGenerator().generate()
        );
    
        projectCategoryRepository.save(projectCategory);
    }

    @Test
    public void testFindByProjectCategoryId() {
        ProjectCategory projectCategory = projectCategoryRepository.findByProjectCategoryId(
            projectCategoryId
        ).orElse(null);

        assertThat(projectCategory.getProjectCategoryId().equals(projectCategoryId)).isTrue();
    }

    @Test
    public void testFindAll() {
        List<ProjectCategory> projectCategories = projectCategoryRepository.findAll();

        assertThat(projectCategories.isEmpty()).isFalse();
    }

    @After
    public void setDone() {
        ProjectCategory projectCategory = projectCategoryRepository.findByProjectCategoryId(
            projectCategoryId
        ).orElse(null);

        projectCategoryRepository.delete(projectCategory);
    }

}