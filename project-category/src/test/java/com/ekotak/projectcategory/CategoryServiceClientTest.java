package com.ekotak.projectcategory;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.UUID;

import com.ekotak.projectcategory.domain.Category;
import com.ekotak.projectcategory.exceptions.ObjectException;
import com.ekotak.projectcategory.services.CategoryServiceClient;
import com.ekotak.projectcategory.services.CategoryServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.uuid.Generators;
import com.netflix.loadbalancer.Server;
import com.netflix.loadbalancer.ServerList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.cloud.netflix.ribbon.RibbonAutoConfiguration;
import org.springframework.cloud.netflix.ribbon.StaticServerList;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.cloud.openfeign.ribbon.FeignRibbonClientAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.Environment;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureWireMock(port = 0)
public class CategoryServiceClientTest {

    private static ObjectMapper mapper = new ObjectMapper();

    @Configuration
	@EnableFeignClients(clients = {CategoryServiceClient.class})
	@ImportAutoConfiguration({
			HttpMessageConvertersAutoConfiguration.class,
			RibbonAutoConfiguration.class,
			FeignRibbonClientAutoConfiguration.class,
			FeignAutoConfiguration.class})
	@Import(CategoryServiceImpl.class)
	static class ContextConfiguration {

		@Autowired
		Environment env;

		@Bean
		ServletWebServerFactory servletWebServerFactory(){
			return new TomcatServletWebServerFactory();
		}

		@Bean
		public ServerList<Server> ribbonServerList() {
			return new StaticServerList<>(new Server("localhost", Integer.valueOf(this.env.getProperty("wiremock.server.port"))));
		}
	}
	
	@Autowired 
	CategoryServiceClient categoryServiceClient;
	
    @Autowired 
    CategoryServiceImpl categoryServiceImpl;

    @WithMockUser(value = "spring")
	@Test
	public void  testGet() throws Exception {
        UUID uuid = Generators.timeBasedGenerator().generate();
        Category category = new Category(uuid, "test");
        String expected = mapper.writeValueAsString(category);
        
		stubFor(get(urlEqualTo("/categories/" + uuid.toString()))
               .willReturn(aResponse()
						.withHeader("Content-Type", "application/json")
						.withBody(expected)
						.withStatus(200)
				)
		);

		String actual = mapper.writeValueAsString(categoryServiceImpl.get(uuid));
		assertThat(expected.equals(actual)).isTrue();
    }

	@WithMockUser(value = "spring")
	@Test
	public void  testGetNotFound() throws Exception {	
		stubFor(get(urlEqualTo("/categories/" + Generators.timeBasedGenerator().generate()))
               .willReturn(aResponse()
						.withHeader("Content-Type", "application/json")
						.withStatus(404)
				)
		);
		
		try {
			categoryServiceImpl.get(Generators.timeBasedGenerator().generate());
		 } catch (ObjectException ex) {
			assertThat(ex.getHttpStatus().value()==404).isTrue();
		}
        
    }
}