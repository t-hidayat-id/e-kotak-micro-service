package com.ekotak.projectcategory;

import static org.assertj.core.api.Assertions.assertThat;

import com.ekotak.projectcategory.exceptions.ObjectException;
import com.ekotak.projectcategory.services.CategoryServiceClient;
import com.ekotak.projectcategory.services.CategoryServiceImpl;
import com.fasterxml.uuid.Generators;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.cloud.netflix.ribbon.RibbonAutoConfiguration;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.cloud.openfeign.ribbon.FeignRibbonClientAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.Environment;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureWireMock(port = 0)
public class CategoryServiceClientUnavailableTest {

    @Configuration
	@EnableFeignClients(clients = {CategoryServiceClient.class})
	@ImportAutoConfiguration({
			HttpMessageConvertersAutoConfiguration.class,
			RibbonAutoConfiguration.class,
			FeignRibbonClientAutoConfiguration.class,
			FeignAutoConfiguration.class})
	@Import(CategoryServiceImpl.class)
	static class ContextConfiguration {

		@Autowired
		Environment env;

		@Bean
		ServletWebServerFactory servletWebServerFactory(){
			return new TomcatServletWebServerFactory();
		}

	}
	
	@Autowired 
	CategoryServiceClient categoryServiceClient;
	
    @Autowired 
    CategoryServiceImpl categoryServiceImpl;

	@WithMockUser(value = "spring")
	@Test
	public void  testGetServiceUnavailable() throws Exception {		
		try {
			categoryServiceImpl.get(Generators.timeBasedGenerator().generate());
		 } catch (ObjectException ex) {
			assertThat(ex.getHttpStatus().value()==503).isTrue();
		}
        
    }
}