package com.ekotak.projectcategory;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.UUID;

import com.ekotak.projectcategory.domain.Category;
import com.ekotak.projectcategory.services.CategoryService;
import com.ekotak.projectcategory.services.CategoryServiceClient;
import com.fasterxml.uuid.Generators;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProjectCategoryApplication.class)
public class CategoryServiceTests {

    @Autowired
    CategoryService categoryService;
    UUID categoryId;

    @MockBean
    private CategoryServiceClient categoryServiceClient;

    @Test
    public void testGet() {
        categoryId = Generators.timeBasedGenerator().generate();

        when(categoryServiceClient.findByCategoryId(categoryId.toString()))
                .thenReturn(new Category(categoryId, "Test"));

        Category category = categoryService.get(categoryId);

        assertThat(category.getCategoryId().equals(categoryId)).isTrue();
    }

}