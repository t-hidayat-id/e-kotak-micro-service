package com.ekotak.projectcategory;

import static org.mockito.Mockito.when;

import java.util.UUID;

import com.ekotak.projectcategory.controllers.ProjectCategoryController;
import com.ekotak.projectcategory.domain.Profile;
import com.ekotak.projectcategory.domain.Project;
import com.ekotak.projectcategory.models.ProjectCategoryPostModel;
import com.ekotak.projectcategory.services.ProfileService;
import com.ekotak.projectcategory.services.ProjectCategoryService;
import com.ekotak.projectcategory.services.ProjectService;
import com.ekotak.projectcategory.services.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@WebMvcTest(ProjectCategoryController.class)
public class ProjectCategoryControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @MockBean
    private ProjectCategoryService projectCategoryService;

    @MockBean 
    private ProfileService profileService;

    @MockBean 
    private ProjectService projectService;

    @WithMockUser(value = "spring")
    @Test
    public void testCreate() throws Exception {
        UUID uuid = UUID.fromString("abece9b4-d74b-11e9-af74-9734ea6fa523");
                
        ProjectCategoryPostModel projectCategoryPostModel = new ProjectCategoryPostModel();
        projectCategoryPostModel.setProjectId(uuid);
        projectCategoryPostModel.setCategoryId(uuid);

        Profile profile = new Profile();
        profile.setProfileId(uuid);
        when(profileService.get()).thenReturn(profile);

        Project project = new Project();
        project.setProfileId(profile.getProfileId());
        when(projectService.findByProjectId(uuid)).thenReturn(project);

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(MockMvcRequestBuilders.post("/project-categories")
                .content(objectMapper.writeValueAsString(projectCategoryPostModel))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }
    
}