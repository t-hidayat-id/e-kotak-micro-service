package com.ekotak.projectcategory;

import static org.assertj.core.api.Assertions.*;

import java.util.UUID;

import com.ekotak.projectcategory.domain.ProjectCategory;
import com.ekotak.projectcategory.services.ProjectCategoryService;
import com.fasterxml.uuid.Generators;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProjectCategoryApplication.class)
@EmbeddedKafka
public class ProjectCatgoryServiceTest {

    @Autowired
    ProjectCategoryService projectCategoryService;
    UUID projectCategoryId;

    @Test
    public void testCreateDeleteFind() {
       ProjectCategory projectCategory = new ProjectCategory(
           null, 
           Generators.timeBasedGenerator().generate(), 
           Generators.timeBasedGenerator().generate()
        );
        projectCategory = projectCategoryService.create(projectCategory);

        projectCategoryId = projectCategory.getProjectCategoryId();
        assertThat(projectCategoryId.toString().isEmpty()).isFalse();

        boolean deleteProjectCategory = projectCategoryService.delete(projectCategory.getProjectCategoryId());
        assertThat(deleteProjectCategory).isTrue();
    }

}