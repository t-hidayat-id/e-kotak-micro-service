package com.ekotak.projectdonation.repositories;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.ekotak.projectdonation.domain.ProjectDonation;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectDonationRepository extends CrudRepository<ProjectDonation, UUID> {
   
    Optional<ProjectDonation> findByProjectDonationtId(UUID projectDonationId);
    List<ProjectDonation> findAll(); 

}