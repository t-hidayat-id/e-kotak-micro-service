package com.ekotak.projectdonation.domain;

import java.io.Serializable;
import java.util.UUID;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import lombok.Data;

@Data
@Table("project_donation")
public class ProjectDonation implements Serializable {

    private static final long serialVersionUID = -6055299955950900198L;

    @PrimaryKey("project_donation_id")
    UUID projectDonationId;

    @Column("project_id")
    UUID projectId;

    @Column("profile_id")
    UUID profileId;

    @Column("currency")
    String currency;

    @Column("value")
    Double value; 

    public ProjectDonation(UUID projectDonationId, UUID projectId, UUID profileId, String currency, Double value) {
        this.projectDonationId = projectDonationId;
        this.projectId = projectId;
        this.profileId = profileId;
        this.currency = currency;
        this.value = value;
    }

}