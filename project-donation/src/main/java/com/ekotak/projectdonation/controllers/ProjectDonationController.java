package com.ekotak.projectdonation.controllers;

import javax.validation.Valid;

import com.ekotak.projectdonation.domain.ProjectDonation;
import com.ekotak.projectdonation.models.ProjectDonationPostModel;
import com.ekotak.projectdonation.services.ProjectDonationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/project-donation")
public class ProjectDonationController {

    @Autowired
    ProjectDonationService projectDonationService;

    @PostMapping(
        consumes = { 
            MediaType.APPLICATION_JSON_VALUE 
        },
		produces = { 
            MediaType.APPLICATION_JSON_VALUE 
        }
	)
    public ResponseEntity<Object> create(
        @Valid @RequestBody ProjectDonationPostModel projectDonationPostModel
    ) {
        ProjectDonation projectDonation = new ProjectDonation(
            null, 
            projectDonationPostModel.getProjectId(),
            null, 
            projectDonationPostModel.getCurrency(),
            projectDonationPostModel.getValue());

        return ResponseEntity.status(HttpStatus.CREATED).body(
            projectDonationService.create(projectDonation)
        );
    }
}