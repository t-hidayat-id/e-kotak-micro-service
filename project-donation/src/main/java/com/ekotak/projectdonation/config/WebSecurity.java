package com.ekotak.projectdonation.config;

import com.ekotak.projectdonation.security.AuthorizationFilter;
import com.ekotak.projectdonation.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter {

    private final Environment environment;
    UserService userService;

    @Autowired
    public WebSecurity(Environment environment, UserService userService) {
        this.environment = environment;
        this.userService = userService;
    }
	
    @Override
    protected void configure(HttpSecurity http) throws Exception {

    	http.csrf().disable(); 
    	http.headers().frameOptions().disable();
        http.authorizeRequests()
        .anyRequest().authenticated()
        .and()
    	.addFilter(new AuthorizationFilter(
                authenticationManager(), environment, userService
            )
        );
    	
    	http.sessionManagement().sessionCreationPolicy(
            SessionCreationPolicy.STATELESS
        );
    	
    }	
	
}
