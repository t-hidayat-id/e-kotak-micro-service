package com.ekotak.projectdonation.models;


import java.util.UUID;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class ProjectDonationPostModel {

    @NotNull(message = "Project Id cannot be null.")
    UUID projectId;

    @NotNull(message = "Currency cannot be null.")
    String currency;

    @NotNull(message = "Value cannot be null.")
    Double value;

}