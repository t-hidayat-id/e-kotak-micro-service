package com.ekotak.projectdonation.services;

import com.ekotak.projectdonation.domain.Profile;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name="profile-mc")
public interface ProfileServiceClient {

    @GetMapping("/profiles/{userId}")
    public Profile findByUserId(@PathVariable String userId);

}