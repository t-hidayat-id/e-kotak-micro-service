package com.ekotak.projectdonation.services;

import com.ekotak.projectdonation.domain.Profile;

public interface ProfileService {

    Profile get();

}