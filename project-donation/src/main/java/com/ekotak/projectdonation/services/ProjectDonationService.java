package com.ekotak.projectdonation.services;

import java.util.UUID;

import com.ekotak.projectdonation.domain.ProjectDonation;

public interface ProjectDonationService {

    ProjectDonation create(ProjectDonation projectDonation);
    boolean delete(ProjectDonation projectDonation);
    ProjectDonation findByProjectDonationId(UUID projectDonationId);

}