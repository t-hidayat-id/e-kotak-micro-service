package com.ekotak.projectdonation.services;

import java.util.UUID;

import com.ekotak.projectdonation.domain.Profile;
import com.ekotak.projectdonation.domain.ProjectDonation;
import com.ekotak.projectdonation.repositories.ProjectDonationRepository;
import com.fasterxml.uuid.Generators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectDonationServiceImpl implements ProjectDonationService {

    ProjectDonationRepository projectDonationRepository;
    ProfileService profileService;

    @Autowired
    public ProjectDonationServiceImpl(ProjectDonationRepository projectDonationRepository) {
        this.projectDonationRepository = projectDonationRepository;
    }

    @Override
    public ProjectDonation create(ProjectDonation projectDonation) {
        UUID projectDonationId = Generators.timeBasedGenerator().generate();
        projectDonation.setProjectDonationId(projectDonationId);
        
        Profile profile = profileService.get();
        projectDonation.setProfileId(profile.getProfileId());

        projectDonation = projectDonationRepository.save(projectDonation);
        return projectDonation;
    }

    @Override
    public boolean delete(ProjectDonation projectDonation) {        
        projectDonationRepository.delete(projectDonation);
        
        return true;
    }

    @Override
    public ProjectDonation findByProjectDonationId(UUID projectDonationId) {
        ProjectDonation projectDonation = projectDonationRepository.findByProjectDonationtId(
            projectDonationId
        ).orElse(null);

        return projectDonation;
    }

}