package com.ekotak.projectdonation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectDonationApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectDonationApplication.class, args);
	}

}
