package com.ekotak.projectdonation;

import static org.assertj.core.api.Assertions.*;

import java.util.List;
import java.util.UUID;

import com.ekotak.projectdonation.domain.ProjectDonation;
import com.ekotak.projectdonation.repositories.ProjectDonationRepository;
import com.fasterxml.uuid.Generators;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProjectDonationApplication.class)
public class ProjectDonationRepositoryTests {

    @Autowired
    ProjectDonationRepository projectDonationRepository;
    UUID projectDonationId;

    @Before
    public void setUp() {
        projectDonationId = Generators.timeBasedGenerator().generate();
        
        ProjectDonation projectDonation = new ProjectDonation(
            projectDonationId, 
            Generators.timeBasedGenerator().generate(), 
            Generators.timeBasedGenerator().generate(),
            "USD",
            1000.00
        );
    
        projectDonationRepository.save(projectDonation);
    }

    @Test
    public void findByProjectDonationId() {
        ProjectDonation projectDonation = projectDonationRepository.findByProjectDonationtId(projectDonationId)
                .orElse(null);

        assertThat(projectDonation.getProjectDonationId().equals(projectDonationId)).isTrue();
    }

    @Test
    public void findAll() {
        List<ProjectDonation> projectDonations = projectDonationRepository.findAll();

        assertThat(projectDonations.isEmpty()).isFalse();
    }

    @After
    public void setDone() {
        ProjectDonation projectDonation = projectDonationRepository.findByProjectDonationtId(
            projectDonationId
        ).orElse(null);

        projectDonationRepository.delete(projectDonation);
    }

}