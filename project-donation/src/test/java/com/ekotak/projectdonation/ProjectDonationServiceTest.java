package com.ekotak.projectdonation;

import static org.assertj.core.api.Assertions.*;

import java.util.UUID;

import com.ekotak.projectdonation.domain.ProjectDonation;
import com.ekotak.projectdonation.services.ProjectDonationService;
import com.fasterxml.uuid.Generators;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProjectDonationApplication.class)
public class ProjectDonationServiceTest {

    @Autowired
    ProjectDonationService projectDonationService;
    UUID projectDonationId;

    @Test
    public void createDeleteFind() {
       ProjectDonation projectDonation = new ProjectDonation(
           null, 
           Generators.timeBasedGenerator().generate(),
           Generators.timeBasedGenerator().generate(),
           "USD",
           1000.00
        );
        projectDonation = projectDonationService.create(projectDonation);

        projectDonationId = projectDonation.getProjectDonationId();
        assertThat(projectDonationId.toString().isEmpty()).isFalse();

        projectDonationService.delete(projectDonation);

        projectDonation = projectDonationService.findByProjectDonationId(projectDonationId);
        assertThat(projectDonation).isNull();
    }

}