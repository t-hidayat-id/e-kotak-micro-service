package com.ekotak.withdraw;

import com.ekotak.withdraw.controllers.WithdrawController;
import com.ekotak.withdraw.services.WithdrawService;
import com.ekotak.withdraw.services.UserService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@WebMvcTest(WithdrawController.class)
public class WithdrawControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @MockBean
    private WithdrawService withdrawService;

    @Test
    public void create() throws Exception {
        String withdraw = "{\"withdrawId\": \"abece9b4-d74b-11e9-af74-9734ea6fa523\", \"withdrawId\" : \"abece9b4-d74b-11e9-af74-9734ea6fa523\"}";
        String token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiI5YTlmN2U5Zi1hYjM5LTExZTktYjVmYi03MzZjMmI2OWNiYWIiLCJleHAiOjE1NzA1ODIxNDh9.t_kU7KH1tfwL_FIT__jNGV1JU6uXqfiVE2kyK092wzud2p5rXZ_h9AZrpuNxKpL_Oqi5EzIesktGJhdIh83QSA";
        
        mockMvc.perform(MockMvcRequestBuilders.post("/withdraw")
                .header("Authorization", "Bearer " + token )
                .content(withdraw)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }
    
}