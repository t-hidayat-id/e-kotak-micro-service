package com.ekotak.withdraw;

import static org.assertj.core.api.Assertions.*;

import java.util.UUID;

import com.ekotak.withdraw.domain.Withdraw;
import com.ekotak.withdraw.services.WithdrawService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WithdrawApplication.class)
public class WithdrawServiceTest {

    @Autowired
    WithdrawService withdrawService;
    UUID withdrawId;

    @Test
    public void createDeleteFind() {
        Withdraw withdraw = new Withdraw(
           null, 
           null,
           "USD",
           1000.00
        );
        withdraw = withdrawService.create(withdraw);

        withdrawId = withdraw.getWithdrawId();
        assertThat(withdrawId.toString().isEmpty()).isFalse();

        withdrawService.delete(withdraw);

        withdraw = withdrawService.findByWithdrawId(withdrawId);
        assertThat(withdraw).isNull();
    }

}