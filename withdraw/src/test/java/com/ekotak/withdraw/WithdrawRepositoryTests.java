package com.ekotak.withdraw;

import static org.assertj.core.api.Assertions.*;

import java.util.List;
import java.util.UUID;

import com.ekotak.withdraw.domain.Withdraw;
import com.ekotak.withdraw.repositories.WithdrawRepository;
import com.fasterxml.uuid.Generators;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WithdrawApplication.class)
public class WithdrawRepositoryTests {

    @Autowired
    WithdrawRepository withdrawRepository;
    UUID withdrawId;

    @Before
    public void setUp() {
        withdrawId = Generators.timeBasedGenerator().generate();
        
        Withdraw withdraw = new Withdraw(
            null, 
            null, 
            "USD",
            1000.00
        );
    
        withdrawRepository.save(withdraw);
    }

    @Test
    public void findByWithdraw() {
        Withdraw withdraw = withdrawRepository.findByWithdrawId(
            withdrawId
        ).orElse(null);

        assertThat(withdraw.getWithdrawId().equals(withdrawId)).isTrue();
    }

    @Test
    public void findAll() {
        List<Withdraw> withdraws = withdrawRepository.findAll();

        assertThat(withdraws.isEmpty()).isFalse();
    }

    @After
    public void setDone() {
        Withdraw withdraw = withdrawRepository.findByWithdrawId(
            withdrawId
        ).orElse(null);

        withdrawRepository.delete(withdraw);
    }
}