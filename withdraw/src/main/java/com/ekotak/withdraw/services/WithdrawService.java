package com.ekotak.withdraw.services;

import java.util.UUID;

import com.ekotak.withdraw.domain.Withdraw;

public interface WithdrawService {

    Withdraw create(Withdraw withdraw);
    boolean delete(Withdraw withdraw);
    Withdraw findByWithdrawId(UUID withdrawId);

}