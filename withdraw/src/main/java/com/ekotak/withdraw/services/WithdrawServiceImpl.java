package com.ekotak.withdraw.services;

import java.util.UUID;

import com.ekotak.withdraw.domain.Profile;
import com.ekotak.withdraw.domain.Withdraw;
import com.ekotak.withdraw.repositories.WithdrawRepository;
import com.fasterxml.uuid.Generators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WithdrawServiceImpl implements WithdrawService {

    WithdrawRepository withdrawRepository;
    ProfileService profileService;

    @Autowired
    public WithdrawServiceImpl(WithdrawRepository withdrawRepository) {
        this.withdrawRepository = withdrawRepository;
    }

    @Override
    public Withdraw create(Withdraw withdraw) {
        UUID withdrawId = Generators.timeBasedGenerator().generate();
        withdraw.setWithdrawId(withdrawId);
        
        Profile profile = profileService.get();
        withdraw.setProfileId(profile.getProfileId());

        withdraw = withdrawRepository.save(withdraw);
        return withdraw;
    }

    @Override
    public boolean delete(Withdraw withdraw) {        
        withdrawRepository.delete(withdraw);
        
        return true;
    }

    @Override
    public Withdraw findByWithdrawId(UUID withdrawId) {
        Withdraw withdraw = withdrawRepository.findByWithdrawId(
            withdrawId
        ).orElse(null);

        return withdraw;
    }

}