package com.ekotak.withdraw.services;

import com.ekotak.withdraw.domain.Profile;

public interface ProfileService {

    Profile get();

}