package com.ekotak.withdraw.domain;

import java.io.Serializable;
import java.util.UUID;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import lombok.Data;

@Data
@Table("withdraw")
public class Withdraw implements Serializable {

    private static final long serialVersionUID = -6055299955950900198L;

    @PrimaryKey("withdraw_id")
    UUID withdrawId;

    @Column("user_id")
    UUID userId;

    @Column("currency")
    String currency;

    @Column("value")
    Double value;


    public Withdraw(UUID withdrawId,UUID userId, String currency, Double value) {
        this.withdrawId = withdrawId;
        this.userId = userId;
        this.currency = currency;
        this.value = value;
    }


	public Withdraw(Object withdrawId2, Object userId2, String currency2, Double value2) {
	}


	public void setProfileId(UUID profileId) {
	}

}