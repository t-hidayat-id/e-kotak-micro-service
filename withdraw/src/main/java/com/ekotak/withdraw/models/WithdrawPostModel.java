package com.ekotak.withdraw.models;

import java.util.UUID;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class WithdrawPostModel {

    @NotNull(message = "Withdraw Id cannot be null.")
    UUID withdrawId;

    @NotNull(message = "User Id cannot be null.")
    UUID userId;

    @NotNull(message = "Currency cannot be null.")
    String currency;

    @NotNull(message = "Value cannot be null.")
    Double value;

}