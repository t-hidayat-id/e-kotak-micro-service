package com.ekotak.withdraw.repositories;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.ekotak.withdraw.domain.Withdraw;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WithdrawRepository extends CrudRepository<Withdraw, UUID> {
   
    Optional<Withdraw> findByWithdrawId(UUID withdrawId);
    List<Withdraw> findAll(); 

}