package com.ekotak.withdraw.security;

import com.ekotak.withdraw.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import feign.RequestInterceptor;
import feign.RequestTemplate;

@Component
public class HeaderRequestInterceptor implements RequestInterceptor {

    @Autowired
    UserService userService;

    @Override
    public void apply(RequestTemplate requestTemplate) {
        requestTemplate.header("Content-Type", "application/json");
        requestTemplate.header(
            "Authorization", 
            String.format("Bearer %s", this.userService.getToken())
        );
    }

}