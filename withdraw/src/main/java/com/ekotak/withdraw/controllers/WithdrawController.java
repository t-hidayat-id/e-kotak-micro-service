package com.ekotak.withdraw.controllers;

import javax.validation.Valid;

import com.ekotak.withdraw.domain.Withdraw;
import com.ekotak.withdraw.models.WithdrawPostModel;
import com.ekotak.withdraw.services.WithdrawService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/withdraw")
public class WithdrawController {

    @Autowired
    WithdrawService withdrawService;

    @PostMapping(
        consumes = { 
            MediaType.APPLICATION_JSON_VALUE 
        },
		produces = { 
            MediaType.APPLICATION_JSON_VALUE 
        }
	)
    public ResponseEntity<Object> create(
        @Valid @RequestBody WithdrawPostModel withdrawPostModel
    ) {
        Withdraw withdraw = new Withdraw(
            null,
            null,
            withdrawPostModel.getCurrency(),
            withdrawPostModel.getValue());

        return ResponseEntity.status(HttpStatus.CREATED).body(
            withdrawService.create(withdraw)
        );
    }
}