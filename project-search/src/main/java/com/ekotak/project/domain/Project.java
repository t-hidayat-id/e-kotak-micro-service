package com.ekotak.project.domain;

import java.io.Serializable;
import java.util.UUID;

import lombok.Data;

@Data
public class Project implements Serializable {

    private static final long serialVersionUID = -1440143802505688103L;

    UUID projectId;

    UUID profileId;

    String slug;

    String title;

    String description;

    public Project() {}

    public Project(String title, String description) {
        this.title = title;
        this.description = description;
    }

}