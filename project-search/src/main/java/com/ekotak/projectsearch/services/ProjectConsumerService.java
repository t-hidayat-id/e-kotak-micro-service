package com.ekotak.projectsearch.services;

import com.ekotak.project.domain.Project;
import com.ekotak.projectsearch.cores.ProjectCore;
import com.ekotak.projectsearch.repositories.ProjectCoreRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
class ProjectConsumerService {
	
	ProjectCoreRepository projectCoreRepository;

	@Autowired
	public ProjectConsumerService(
		ProjectCoreRepository projectCoreRepository
	) {
		this.projectCoreRepository = projectCoreRepository;
	}
	
    @KafkaListener(topics = "projectTopic")
	public void processMessage(Project project) {	
		ProjectCore projectCore = new ProjectCore(
			project.getProjectId().toString(),  
			project.getProfileId().toString(),
			project.getSlug(), 
			project.getTitle(), 
			project.getDescription()
		);

		this.projectCoreRepository.save(projectCore);
	}

}