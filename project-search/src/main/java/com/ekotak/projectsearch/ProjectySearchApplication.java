package com.ekotak.projectsearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectySearchApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectySearchApplication.class, args);
	}

}
