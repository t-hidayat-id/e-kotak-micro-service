package com.ekotak.projectsearch.repositories;

import com.ekotak.projectsearch.cores.ProjectCore;

import org.springframework.data.solr.repository.SolrCrudRepository;

public interface ProjectCoreRepository 
    extends SolrCrudRepository<ProjectCore, String> 
{
    
}