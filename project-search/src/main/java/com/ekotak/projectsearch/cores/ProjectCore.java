package com.ekotak.projectsearch.cores;

import java.io.Serializable;

import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.SolrDocument;

import lombok.Data;

@Data
@SolrDocument(solrCoreName = "project")
public class ProjectCore implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
	@Field
    private String id;
    
    @Field("project_id")
    String projectId;

    @Field("profile_id")
    String profileId;

    @Field
    String slug;
    
    @Field
    String title;

    @Field
    String description;

    public ProjectCore() {}

    public ProjectCore(
        String projectId, 
        String profileID,
        String slug,
        String title, 
        String description
    ) {
        this.id = projectId;
        this.projectId = projectId;
        this.profileId = profileID;
        this.slug = slug;
        this.title = title;
        this.description = description;
    }

}