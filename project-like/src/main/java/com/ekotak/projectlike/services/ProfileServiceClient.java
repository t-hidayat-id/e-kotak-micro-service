package com.ekotak.projectcomment.services;

import com.ekotak.projectcomment.domain.Profile;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name="profile-mc")
public interface ProfileServiceClient {

    @GetMapping("/profiles/{userId}")
    public Profile findByUserId(@PathVariable String userId);

}