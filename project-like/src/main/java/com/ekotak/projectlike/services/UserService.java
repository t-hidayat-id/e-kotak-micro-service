package com.ekotak.projectcomment.services;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import lombok.Data;

@Service
@Data
public class UserService {

    private String token;
    
    public String getId() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }
    
}