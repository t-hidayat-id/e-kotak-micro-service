package com.ekotak.projectcomment.services;

import java.util.UUID;

import com.ekotak.projectcomment.domain.ProjectComment;

public interface ProjectCommentService {

    ProjectComment create(ProjectComment projectComment);
    boolean delete(ProjectComment projectComment);
    ProjectComment findByProjectCommentId(UUID projectCommentId);

}