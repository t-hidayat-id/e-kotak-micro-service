package com.ekotak.projectlike.controllers;

import javax.validation.Valid;

import com.ekotak.projectlike.domain.ProjectLike;
import com.ekotak.projectlike.models.ProjectLikePostModel;
import com.ekotak.projectlike.services.ProjectLikeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/project-like")
public class ProjectLikeController {

    @Autowired
    ProjectLikeService projectLikeService;

    @PostMapping(
        consumes = { 
            MediaType.APPLICATION_JSON_VALUE 
        },
		produces = { 
            MediaType.APPLICATION_JSON_VALUE 
        }
	)
    public ResponseEntity<Object> create(
        @Valid @RequestBody ProjectLikePostModel projectLikePostModel
    ) {
        ProjectLike projectLike = new ProjectLike(
            null, 
            projectLikePostModel.getProjectId(),
            null, 
            projectLikePostModel.getLike());

        return ResponseEntity.status(HttpStatus.CREATED).body(
            projectLikeService.create(projectLike)
        );
    }
}