package com.ekotak.projectdonation.domain;

import java.util.UUID;

import lombok.Data;

@Data
public class Profile {

    private UUID profileId;
    private UUID userId;
    private String displayName;
    private String linkId;
    private String description;

    public Profile() {};
    
}