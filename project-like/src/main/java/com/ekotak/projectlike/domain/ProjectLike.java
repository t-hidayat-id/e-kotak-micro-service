package com.ekotak.projectlike.domain;

import java.io.Serializable;
import java.util.UUID;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import lombok.Data;

@Data
@Table("project_like")
public class ProjectLike implements Serializable {

    private static final long serialVersionUID = -6055299955950900198L;

    @PrimaryKey("project_like_id")
    UUID projectLike;

    @Column("project_id")
    UUID projectId;

    @Column("profile_id")
    UUID profileId;

    @Column("like")
    Boolean like;

    public ProjectLike(UUID projectLikeId, UUID projectId, UUID profileId, Boolean like) {
        this.projectLikeId = projectLikeId;
        this.projectId = projectId;
        this.profileId = profileId;
        this.like = like;
    }

}