package com.ekotak.projectlike.models;

import java.util.UUID;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class ProjectDonationPostModel {

    @NotNull(message = "Project Id cannot be null.")
    UUID projectId;

    @NotNull(message = "Like cannot be null.")
    Boolean like;

}