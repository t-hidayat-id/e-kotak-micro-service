package com.ekotak.projectlike.repositories;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.ekotak.projectlike.domain.ProjectLike;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectLikeRepository extends CrudRepository<ProjectLike, UUID> {
   
    Optional<ProjectLike> findByProjectLikeId(UUID projectLikeId);
    List<ProjectLike> findAll(); 

}