package com.ekotak.projectlike;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectLikeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectLikepplication.class, args);
	}

}
