package com.ekotak.projectlike;

import static org.assertj.core.api.Assertions.*;

import java.util.UUID;

import com.ekotak.projectlike.domain.ProjectLike;
import com.ekotak.projectlike.services.ProjectLikeService;
import com.fasterxml.uuid.Generators;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProjectLikeApplication.class)
public class ProjectLikeServiceTest {

    @Autowired
    ProjectLikeService projectLikeService;
    UUID projectLikeId;

    @Test
    public void createDeleteFind() {
       ProjectLike projectLike = new ProjectLike(
           null, 
           Generators.timeBasedGenerator().generate(),
           Generators.timeBasedGenerator().generate(),
           "Like"
        );
        projectLike = projectLikeService.create(projectLike);

        projectLikeId = projectLike.getProjectLikeId();
        assertThat(projectLikeId.toString().isEmpty()).isFalse();

        projectLikeService.delete(projectLike);

        projectLike = projectLikeService.findByProjectLikeId(projectLikeId);
        assertThat(projectLike).isNull();
    }

}