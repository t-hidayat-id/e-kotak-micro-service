package com.ekotak.projectlike;

import static org.assertj.core.api.Assertions.*;

import java.util.List;
import java.util.UUID;

import com.ekotak.projectlike.domain.ProjectLike;
import com.ekotak.projectlike.repositories.ProjectLikeRepository;
import com.fasterxml.uuid.Generators;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProjectLikeApplication.class)
public class ProjectLikeRepositoryTests {

    @Autowired
    ProjectLikeRepository projectLikeRepository;
    UUID projectLikeId;

    @Before
    public void setUp() {
        projectLikeId = Generators.timeBasedGenerator().generate();
        
        ProjectLike projectLike = new ProjectLike(
            projectLikeId, 
            Generators.timeBasedGenerator().generate(), 
            Generators.timeBasedGenerator().generate(),
            "Like"
        );
    
        projectLikeRepository.save(projectLike);
    }

    @Test
    public void findByProjectLike() {
        ProjectLike projectLike = projectLikeRepository.findByProjectLikeId(
            projectLikeId
        ).orElse(null);

        assertThat(projectLike.getProjectLikeId().equals(projectLikeId)).isTrue();
    }

    @Test
    public void findAll() {
        List<ProjectLike> projectLikes = projectLikeRepository.findAll();

        assertThat(projectLikes.isEmpty()).isFalse();
    }

    @After
    public void setDone() {
        ProjectLike projectLike = projectLikeRepository.findByProjectLikeId(
            projectLikeId
        ).orElse(null);

        projectLikeRepository.delete(projectLike);
    }
}