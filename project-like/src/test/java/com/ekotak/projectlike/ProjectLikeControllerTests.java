package com.ekotak.projectlike;

import com.ekotak.projectlike.controllers.ProjectLikeController;
import com.ekotak.projectlike.services.ProjectLikeService;
import com.ekotak.projectlike.services.UserService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@WebMvcTest(ProjectLikeController.class)
public class ProjectLikeControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @MockBean
    private ProjectLikeService ProjectLikeService;

    @Test
    public void create() throws Exception {
        String projectLike = "{\"projectId\": \"abece9b4-d74b-11e9-af74-9734ea6fa523\", \"likeId\" : \"abece9b4-d74b-11e9-af74-9734ea6fa523\"}";
        String token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiI5YTlmN2U5Zi1hYjM5LTExZTktYjVmYi03MzZjMmI2OWNiYWIiLCJleHAiOjE1NzA1ODIxNDh9.t_kU7KH1tfwL_FIT__jNGV1JU6uXqfiVE2kyK092wzud2p5rXZ_h9AZrpuNxKpL_Oqi5EzIesktGJhdIh83QSA";
        
        mockMvc.perform(MockMvcRequestBuilders.post("/project-like")
                .header("Authorization", "Bearer " + token )
                .content(projectLike)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }
    
}