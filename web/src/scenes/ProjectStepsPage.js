import React, { Component } from 'react';
import { connect } from 'react-redux';
import get from 'lodash/get';
import { Menu, Icon } from 'antd';

import { Page, ProjectSteps } from '../components';

const { SubMenu } = Menu;

class ProjectsStepsPage extends Component {
  menuSiderClick = (e) =>  {
    switch (e.key) {
      case "home":
        this.props.history.push('/');
        break;
      default:
        break;
    }
  };

  menuHeaderClick = (e) =>  {
    switch (e.key) {
      case "register":
        this.props.history.push('/register');
        break;
      default:
        break;
    }
  };

  render() {
    const firstName = get(this.props.user.data, 'firstName');
    return (
      <Page
        defaultMenuSiderKey={
          ['login']
        }
        menuSider={
          <Menu.Item key="login">
            <Icon type="login" />
            <span className="nav-text">Sign In</span>
          </Menu.Item>
        }
        menuSiderClick={
          this.menuSiderClick
        }
        menuHeader={
          firstName ? [
            <SubMenu
              key="user"
              title={
                <span>
                  <Icon type="user"/>{firstName}
                </span>
              }
            >
              <Menu.Item key="profile">
                <Icon type="solution"/>Profile
              </Menu.Item>
              <Menu.Divider/>
              <Menu.Item key="logout">
                <Icon type="logout"/>Logout
              </Menu.Item>
            </SubMenu>
          ] : [
            <Menu.Item key="user">
              <Icon type="user"/>Account
            </Menu.Item>
          ]
        }
        menuHeaderClick={
          this.menuHeaderClick
        }
        content={
          <ProjectSteps/>
        }
      />  
    )  
  }
}

const mapStateToProps = (state, ) => {
  return {
    user: state.user
  }
};

export default connect(mapStateToProps)(ProjectsStepsPage);