import React, { Component } from 'react';
import { Menu, Icon } from 'antd';

import { Page, Login } from '../components';

class LoginPage extends Component {
  menuSiderClick = (e) =>  {
    switch (e.key) {
      case "home":
        this.props.history.push('/');
        break;
      default:
        break;
    }
  };

  menuHeaderClick = (e) =>  {
    switch (e.key) {
      case "register":
        this.props.history.push('/register');
        break;
      default:
        break;
    }
  };

  render() {
    return (
      <Page
        defaultMenuSiderKey={
          ['login']
        }
        menuSider={
          <Menu.Item key="login">
            <Icon type="login" />
            <span className="nav-text">Sign In</span>
          </Menu.Item>
        }
        menuSiderClick={
          this.menuSiderClick
        }
        menuHeader={
          <Menu.Item key="register">
            <Icon type="user-add"/>Sign Up
          </Menu.Item>
        }
        menuHeaderClick={
          this.menuHeaderClick
        }
        content={
          <Login/>
        }
      />
    )
  }
}

export default LoginPage;