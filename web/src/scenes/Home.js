import React, { Component } from 'react';
import { connect } from 'react-redux';
import get from 'lodash/get';
import { Menu, Icon } from 'antd';

import * as actions from "../services/actions";
import { Page } from '../components';

const { SubMenu } = Menu;

class Home extends Component {
  menuSiderClick = (e) =>  {
    switch (e.key) {
      case "home":
        this.props.history.push('/');
        break;
      case "add-project":
        this.props.history.push('/project');
        break;
      default:
        break;
    }
  };

  menuHeaderClick = (e) =>  {
    switch (e.key) {
      case "user":
        this.props.history.push('/login');
        break;
      case "profile":
        this.props.history.push('/profile');
        break;
      case "logout":
        localStorage.removeItem('id');
        localStorage.removeItem('jwt');
        this.props.dispatch(actions.loginInitial());
        this.props.dispatch(actions.userInitial());
        this.props.history.push('/login');
        break;
      default:
        break;
    }
  };

  render() {
    const firstName = get(this.props.user.data, 'firstName');
    return (
      <Page
        menuSider={
          [
            <Menu.Item key="add-project">
              <Icon type="plus" />
              <span className="nav-text">Add Project</span>
            </Menu.Item>,
            <Menu.Item key="explorer">
              <Icon type="appstore" />
              <span className="nav-text">Explorer</span>
            </Menu.Item>,
            <Menu.Item key="category">
              <Icon type="bars" />
              <span className="nav-text">Category</span>
            </Menu.Item>
          ]
        }
        menuSiderClick={
          this.menuSiderClick
        }
        menuHeader={
          firstName ? [
            <SubMenu
              key="user"
              title={
                <span>
                  <Icon type="user"/>{firstName}
                </span>
              }
            >
              <Menu.Item key="profile">
                <Icon type="solution"/>Profile
              </Menu.Item>
              <Menu.Divider/>
              <Menu.Item key="logout">
                <Icon type="logout"/>Logout
              </Menu.Item>
            </SubMenu>
          ] : [
            <Menu.Item key="user">
              <Icon type="user"/>Account
            </Menu.Item>
          ]
        }
        menuHeaderClick={
          this.menuHeaderClick
        }
      />
    )
  }
}

const mapStateToProps = (state, ) => {
  return {
    user: state.user
  }
};

export default connect(mapStateToProps)(Home);