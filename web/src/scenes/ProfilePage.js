import React, { Component } from 'react';
import { connect } from 'react-redux';
import get from 'lodash/get';
import { Menu, Icon } from 'antd';

import * as actions from "../services/actions";
import { Page, Profile } from '../components';

const { SubMenu } = Menu;

class ProfilePage extends Component {
  menuSiderClick = (e) =>  {
    switch (e.key) {
      case "home":
        this.props.history.push('/');
        break;
      default:
        break;
    }
  };

  menuHeaderClick = (e) =>  {
    switch (e.key) {
      case "profile":
        this.props.history.push('/profile');
        break;
      case "logout":
        localStorage.removeItem('id');
        localStorage.removeItem('jwt');
        this.props.dispatch(actions.loginInitial());
        this.props.dispatch(actions.userInitial());
        this.props.history.push('/login');
        break;
      default:
        break;
    }
  };

  render() {
    const firstName = get(this.props.user.data, 'firstName');
    return (
      <Page
        defaultMenuSiderKey={
          ['profile']
        }
        menuSider={
          [
            <Menu.Item key="profile">
              <Icon type="solution" />
              <span className="nav-text">Profile</span>
            </Menu.Item>,
          ]
        }
        menuSiderClick={
          this.menuSiderClick
        }
        menuHeader={
          [
            <SubMenu
              key="user"
              title={
                <span>
                  <Icon type="user"/>{firstName}
                </span>
              }
            >
              <Menu.Item key="profile">
                <Icon type="solution"/>Profile
              </Menu.Item>
              <Menu.Divider/>
              <Menu.Item key="logout">
                <Icon type="logout"/>Logout
              </Menu.Item>
            </SubMenu>
          ]
        }
        menuHeaderClick={
          this.menuHeaderClick
        }
        content={
          <Profile/>
        }
      />
    )
  }
}

const mapStateToProps = (state, ) => {
  return {
    user: state.user
  }
};

export default connect(mapStateToProps)(ProfilePage);