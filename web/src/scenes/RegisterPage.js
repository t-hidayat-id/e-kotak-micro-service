import React, { Component } from 'react';
import { Menu, Icon } from 'antd';

import { Page, Register } from '../components';

class LoginPage extends Component {
  menuSiderClick = (e) =>  {
    switch (e.key) {
      case "home":
        this.props.history.push('/');
        break;
      default:
        break;
    }
  };

  menuHeaderClick = (e) =>  {
    switch (e.key) {
      case "login":
        this.props.history.push('/login');
        break;
      default:
        break;
    }
  };

  render() {
    return (
      <Page
        defaultMenuSiderKey={
          ['register']
        }
        menuSider={
          <Menu.Item key="register">
            <Icon type="user-add" />
            <span className="nav-text">Sign Up</span>
          </Menu.Item>
        }
        menuSiderClick={
          this.menuSiderClick
        }
        menuHeader={
          <Menu.Item key="login">
            <Icon type="login"/>Sign In
          </Menu.Item>
        }
        menuHeaderClick={
          this.menuHeaderClick
        }
        content={
          <Register/>
        }
      />
    )
  }
}

export default LoginPage;