import { put, call, takeLatest, all } from "redux-saga/effects";
import * as actions from "../actions/index";
import * as api from "../index";

export function* registerRequest(action) {
  try {
    const response = yield call(
      api.registerRequest,
      action.values
    );
    yield put(actions.registerSuccess(response));
  } catch (error) {
    yield put(actions.registerFailed(error));
  }
}

export function* loginRequest(action) {
  try {
    const response = yield call(
      api.loginRequest,
      action.values
    );
    yield put(actions.loginSuccess(response));
  } catch (error) {
    yield put(actions.loginFailed(error));
  }
}

export function* userRequest() {
  try {
    const response = yield call(
      api.userRequest
    );
    yield put(actions.userSuccess(response));
  } catch (error) {
    yield put(actions.userFailed(error));
  }
}

export function* postProfileRequest(action) {
  try {
    const response = yield call(
      api.postProfileRequest,
      action.values
    );
    yield put(actions.postProfileSuccess(response));
  } catch (error) {
    yield put(actions.postProfileFailed(error));
  }
}

export function* getProfileRequest(action) {
  try {
    const response = yield call(
      api.getProfileRequest
    );
    yield put(actions.getProfileSuccess(response));
  } catch (error) {
    yield put(actions.getProfileFailed(error));
  }
}

export function* putProfileRequest(action) {
  try {
    const response = yield call(
      api.putProfileRequest,
      action.values
    );
    yield put(actions.putProfileSuccess(response));
  } catch (error) {
    yield put(actions.putProfileFailed(error));
  }
}

export function* editProjectRequest(action) {
  yield put(actions.editProjectSuccess(action.values));
}

export function* editProjectCoverRequest(action) {
  yield put(actions.editProjectCoverSuccess(action.values));
}

export function* postProjectRequest(action) {
  try {
    const response = yield call(
      api.postProjectRequest,
      action.values
    );
    yield put(actions.postProjectSuccess(response));
  } catch (error) {
    yield put(actions.postProjectFailed(error));
  }
}

export function* postProjectCoverRequest(action) {
  try {
    const response = yield call(
      api.postProjectCoverRequest,
      action.values
    );
    yield put(actions.postProjectCoverSuccess(response));
  } catch (error) {
    yield put(actions.postProjectCoverFailed(error));
  }
}

export function* postProjectCoverOriginalRequest(action) {
  try {
    const response = yield call(
      api.postProjectCoverOriginalRequest,
      action.values
    );
    yield put(actions.postProjectCoverOriginalSuccess(response));
  } catch (error) {
    yield put(actions.postProjectCoverOriginalFailed(error));
  }
}

function* sagas() {
  yield all ([
    takeLatest("REGISTER_REQUEST", registerRequest),
    takeLatest("LOGIN_REQUEST", loginRequest),
    takeLatest("USER_REQUEST", userRequest),
    takeLatest("POST_PROFILE_REQUEST", postProfileRequest),
    takeLatest("GET_PROFILE_REQUEST", getProfileRequest),
    takeLatest("PUT_PROFILE_REQUEST", putProfileRequest),
    takeLatest("EDIT_PROJECT_REQUEST", editProjectRequest),
    takeLatest("EDIT_PROJECT_COVER_REQUEST", editProjectCoverRequest),
    takeLatest("POST_PROJECT_REQUEST", postProjectRequest),
    takeLatest("POST_PROJECT_COVER_REQUEST", postProjectCoverRequest),
    takeLatest("POST_PROJECT_COVER_ORIGINAL_REQUEST", postProjectCoverOriginalRequest)
  ]);
}

export default sagas;