import update from "immutability-helper";
import * as actions from "../actions/index";

const initial = {
  values: null,
  request: false,
  success: false,
  data: null,
  error: {
    status: null,
    statusText: null,
    result: null
  }
};

function updateInitial(state) {
  return update(state, {
    values: { $set: null },
    request: { $set: false },
    success: { $set: false },
    data: { $set: null },
    error: {
      status: { $set: null },
      statusText: { $set: null },
      result: { $set: null }
    }
  });
}

function updateRequest(state, action) {
  return update(state, {
    values: { $set: action.values },
    request: { $set: true },
    success: { $set: false },
    data: { $set: null },
    error: {
      status: { $set: null },
      statusText: { $set: null },
      result: { $set: null }
    }
  });
}

function updateSuccess(state, action) {
  return update(state, {
    values: { $set: null },
    request: { $set: false },
    success: { $set: true },
    data: { $set: action.data },
    error: {
      status: { $set: null },
      statusText: { $set: null },
      result: { $set: {} }
    }
  });
}

function updateFailed(state, action) {
  return update(state, {
    values: { $set: null },
    request: { $set: false },
    success: { $set: false },
    data: { $set: null },
    error: {
      status: { $set: action.error.status },
      statusText: { $set: action.error.statusText },
      result: { $set: action.error.result }
    }
  });
}

function register(state = initial, action) {
  switch (action.type) {
    case actions.REGISTER_INITIAL:
      return updateInitial(state);
    case actions.REGISTER_REQUEST:
      return updateRequest(state, action);
    case actions.REGISTER_SUCCESS:
      return updateSuccess(state, action);
    case actions.REGISTER_FAILED:
      return updateFailed(state, action);
    default:
      return state;
  }
}

function login(state = initial, action) {
  switch (action.type) {
    case actions.LOGIN_INITIAL:
      return updateInitial(state);
    case actions.LOGIN_REQUEST:
      return updateRequest(state, action);
    case actions.LOGIN_SUCCESS:
      return updateSuccess(state, action);
    case actions.LOGIN_FAILED:
      return updateFailed(state, action);
    default:
      return state;
  }
}

function user(state = initial, action) {
  switch (action.type) {
    case actions.USER_INITIAL:
      return updateInitial(state);
    case actions.USER_REQUEST:
      return updateRequest(state, action);
    case actions.USER_SUCCESS:
      return updateSuccess(state, action);
    case actions.USER_FAILED:
      return updateFailed(state, action);
    default:
      return state;
  }
}

function postProfile(state = initial, action) {
  switch (action.type) {
    case actions.POST_PROFILE_INITIAL:
      return updateInitial(state);
    case actions.POST_PROFILE_REQUEST:
      return updateRequest(state, action);
    case actions.POST_PROFILE_SUCCESS:
      return updateSuccess(state, action);
    case actions.POST_PROFILE_FAILED:
      return updateFailed(state, action);
    default:
      return state;
  }
}

function getProfile(state = initial, action) {
  switch (action.type) {
    case actions.GET_PROFILE_INITIAL:
      return updateInitial(state);
    case actions.GET_PROFILE_REQUEST:
      return updateRequest(state, action);
    case actions.GET_PROFILE_SUCCESS:
      return updateSuccess(state, action);
    case actions.GET_PROFILE_FAILED:
      return updateFailed(state, action);
    default:
      return state;
  }
}

function putProfile(state = initial, action) {
  switch (action.type) {
    case actions.PUT_PROFILE_INITIAL:
      return updateInitial(state);
    case actions.PUT_PROFILE_REQUEST:
      return updateRequest(state, action);
    case actions.PUT_PROFILE_SUCCESS:
      return updateSuccess(state, action);
    case actions.PUT_PROFILE_FAILED:
      return updateFailed(state, action);
    default:
      return state;
  }
}

function editProject(state = initial, action) {
  switch (action.type) {
    case actions.EDIT_PROJECT_INITIAL:
      return updateInitial(state);
    case actions.EDIT_PROJECT_REQUEST:
      return updateRequest(state, action);
    case actions.EDIT_PROJECT_SUCCESS:
      return updateSuccess(state, action);
    default:
      return state;
  }
}

function editProjectCover(state = initial, action) {
  switch (action.type) {
    case actions.EDIT_PROJECT_COVER_INITIAL:
      return updateInitial(state);
    case actions.EDIT_PROJECT_COVER_REQUEST:
      return updateRequest(state, action);
    case actions.EDIT_PROJECT_COVER_SUCCESS:
      return updateSuccess(state, action);
    default:
      return state;
  }
}

function postProject(state = initial, action) {
  switch (action.type) {
    case actions.POST_PROJECT_INITIAL:
      return updateInitial(state);
    case actions.POST_PROJECT_REQUEST:
      return updateRequest(state, action);
    case actions.POST_PROJECT_SUCCESS:
      return updateSuccess(state, action);
    case actions.POST_PROJECT_FAILED:
      return updateFailed(state, action);
    default:
      return state;
  }
}

function postProjectCover(state = initial, action) {
  switch (action.type) {
    case actions.POST_PROJECT_COVER_INITIAL:
      return updateInitial(state);
    case actions.POST_PROJECT_COVER_REQUEST:
      return updateRequest(state, action);
    case actions.POST_PROJECT_COVER_SUCCESS:
      return updateSuccess(state, action);
    case actions.POST_PROJECT_COVER_FAILED:
      return updateFailed(state, action);
    default:
      return state;
  }
}

function postProjectCoverOriginal(state = initial, action) {
  switch (action.type) {
    case actions.POST_PROJECT_COVER_ORIGINAL_INITIAL:
      return updateInitial(state);
    case actions.POST_PROJECT_COVER_ORIGINAL_REQUEST:
      return updateRequest(state, action);
    case actions.POST_PROJECT_COVER_ORIGINAL_SUCCESS:
      return updateSuccess(state, action);
    case actions.POST_PROJECT_COVER_ORIGINAL_FAILED:
      return updateFailed(state, action);
    default:
      return state;
  }
}

const reducers = {
  register,
  login,
  user,
  postProfile,
  getProfile,
  putProfile,
  editProject,
  editProjectCover,
  postProject,
  postProjectCover,
  postProjectCoverOriginal
};

export default reducers;
