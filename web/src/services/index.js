import "isomorphic-fetch";
import assign from "lodash/assign";
import querystring from "querystring";

function handleResponse(response) {
  return response.json().then(result => {
    if (response.ok) {
      return result;
    } else {
      const error = {
        status: response.status,
        statusText: response.statusText,
        result: result
      };
      throw error;
    }
  });
}

function handleRequest(
  url, values = null, method = "get", authorization = false
) {
  let request = {
    method:  method ,
    credentials: "include",
    headers: {
      "Content-Type": "application/json",
      "Accept": "application/json"
    }
  };

  if (method === 'upload') {
    request = {
      method:  'post' ,
      credentials: "include",
      headers: {
        "Accept": "application/json"
      }
    };
  }
  
  if (authorization) {
    request["headers"]["Authorization"] = 
      "Bearer " + localStorage.getItem("jwt");
  }

  if (values) {
    if (method === "post" || method === "put") {
      assign(request, {body: JSON.stringify(values)});
    } else if (method === "upload") {
      assign(request, {body: values});
    } else {
      url = url.concat("?", querystring.stringify(values));
    }
  }
  return fetch(url, request).then(handleResponse);
}

export function registerRequest(values) {
  return handleRequest(
    `/user-mc/users`,
    {
      "firstName": values["firstName"], 
      "lastName": values["lastName"], 
      "email": values["email"], 
      "password": values["password"],
    },
    "post",
    false
  );
}

export function loginRequest(values) {
  return handleRequest(
    `/user-mc/users/login`,
    {
      "email": values["email"], 
      "password": values["password"]
    },
    "post",
    false
  );
}

export function userRequest() {
  return handleRequest(
    `/user-mc/users/${
      localStorage.getItem("id")
    }`,
    null,
    'get',
    true
  );
}

export function postProfileRequest(values) {
  return handleRequest(
    `/profile-mc/profiles`,
    values,
    "post",
    true
  );
}

export function getProfileRequest(values) {
  return handleRequest(
    `/profile-mc/profiles/${
      localStorage.getItem("id")
    }`,
    null,
    'get',
    true
  );
}

export function putProfileRequest(values) {
  return handleRequest(
    `/profile-mc/profiles`,
    values,
    "put",
    true
  );
}

export function postProjectRequest(values) {
  return handleRequest(
    `/project-mc/projects`,
    values,
    "post",
    true
  );
}

export function postProjectCoverRequest(values) {
  return handleRequest(
    `/project-cover-mc/projects/${
      values['projectId']
    }/cover`,
    {
      encodedImage: values['encodedImage'],
      properties: values['properties']
    },
    "post",
    true
  );
}

export function postProjectCoverOriginalRequest(values) {
  var data = new FormData();
  data.append('file',  values['file']);
  return handleRequest(
    `/project-cover-mc/projects/${
      values['projectId']
    }/original`,
    data,
    "upload",
    true
  );
}