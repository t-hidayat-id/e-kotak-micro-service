export const REGISTER_INITIAL = "REGISTER_INITIAL";
export const REGISTER_REQUEST = "REGISTER_REQUEST";
export const REGISTER_SUCCESS = "REGISTER_SUCCESS";
export const REGISTER_FAILED = "REGISTER_FAILED";

export function registerInitial() {
  return {
    type: REGISTER_INITIAL
  };
}

export function registerRequest(values) {
  return {
    type: REGISTER_REQUEST,
    values: values
  };
}

export function registerSuccess(data) {
  return {
    type: REGISTER_SUCCESS,
    data: data
  };
}

export function registerFailed(error) {
  return {
    type: REGISTER_FAILED,
    error: error
  };
}

export const LOGIN_INITIAL = "LOGIN_INITIAL";
export const LOGIN_REQUEST = "LOGIN_REQUEST";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAILED = "LOGIN_FAILED";

export function loginInitial() {
  return {
    type: LOGIN_INITIAL
  };
}

export function loginRequest(values) {
  return {
    type: LOGIN_REQUEST,
    values: values
  };
}

export function loginSuccess(data) {
  localStorage.setItem('id', data['id']);
  localStorage.setItem('jwt', data['jwt']);
  return {
    type: LOGIN_SUCCESS,
    data: data
  };
}

export function loginFailed(error) {
  return {
    type: LOGIN_FAILED,
    error: error
  };
}

export const USER_INITIAL = "USER_INITIAL";
export const USER_REQUEST = "USER_REQUEST";
export const USER_SUCCESS = "USER_SUCCESS";
export const USER_FAILED = "USER_FAILED";

export function userInitial() {
  return {
    type: USER_INITIAL
  };
}

export function userRequest(values) {
  return {
    type: USER_REQUEST
  };
}

export function userSuccess(data) {
  return {
    type: USER_SUCCESS,
    data: data
  };
}

export function userFailed(error) {
  return {
    type: USER_FAILED,
    error: error
  };
}

export const POST_PROFILE_INITIAL = "POST_PROFILE_INITIAL";
export const POST_PROFILE_REQUEST = "POST_PROFILE_REQUEST";
export const POST_PROFILE_SUCCESS = "POST_PROFILE_SUCCESS";
export const POST_PROFILE_FAILED = "POST_PROFILE_FAILED";

export function postProfileInitial() {
  return {
    type: POST_PROFILE_INITIAL
  };
}

export function postProfileRequest(values) {
  return {
    type: POST_PROFILE_REQUEST,
    values: values
  };
}

export function postProfileSuccess(data) {
  return {
    type: POST_PROFILE_SUCCESS,
    data: data
  };
}

export function postProfileFailed(error) {
  return {
    type: POST_PROFILE_FAILED,
    error: error
  };
}

export const GET_PROFILE_INITIAL = "GET_PROFILE_INITIAL";
export const GET_PROFILE_REQUEST = "GET_PROFILE_REQUEST";
export const GET_PROFILE_SUCCESS = "GET_PROFILE_SUCCESS";
export const GET_PROFILE_FAILED = "GET_PROFILE_FAILED";

export function getProfileInitial() {
  return {
    type: GET_PROFILE_INITIAL
  };
}

export function getProfileRequest() {
  return {
    type: GET_PROFILE_REQUEST
  };
}

export function getProfileSuccess(data) {
  return {
    type: GET_PROFILE_SUCCESS,
    data: data
  };
}

export function getProfileFailed(error) {
  return {
    type: GET_PROFILE_FAILED,
    error: error
  };
}

export const PUT_PROFILE_INITIAL = "PUT_PROFILE_INITIAL";
export const PUT_PROFILE_REQUEST = "PUT_PROFILE_REQUEST";
export const PUT_PROFILE_SUCCESS = "PUT_PROFILE_SUCCESS";
export const PUT_PROFILE_FAILED = "PUT_PROFILE_FAILED";

export function putProfileInitial() {
  return {
    type: PUT_PROFILE_INITIAL
  };
}

export function putProfileRequest(values) {
  return {
    type: PUT_PROFILE_REQUEST,
    values: values
  };
}

export function putProfileSuccess(data) {
  return {
    type: PUT_PROFILE_SUCCESS,
    data: data
  };
}

export function putProfileFailed(error) {
  return {
    type: PUT_PROFILE_FAILED,
    error: error
  };
}

export const EDIT_PROJECT_INITIAL = "EDIT_PROJECT_INITIAL";
export const EDIT_PROJECT_REQUEST = "EDIT_PROJECT_REQUEST";
export const EDIT_PROJECT_SUCCESS = "EDIT_PROJECT_SUCCESS";

export function editProjectInitial() {
  return {
    type: EDIT_PROJECT_INITIAL
  };
}

export function editProjectRequest(values) {
  return {
    type: EDIT_PROJECT_REQUEST,
    values: values
  };
}

export function editProjectSuccess(data) {
  return {
    type: EDIT_PROJECT_SUCCESS,
    data: data
  };
}

export const EDIT_PROJECT_COVER_INITIAL = "EDIT_PROJECT_COVER_INITIAL";
export const EDIT_PROJECT_COVER_REQUEST = "EDIT_PROJECT_COVER_REQUEST";
export const EDIT_PROJECT_COVER_SUCCESS = "EDIT_PROJECT_COVER_SUCCESS";

export function editProjectCoverInitial() {
  return {
    type: EDIT_PROJECT_COVER_INITIAL
  };
}

export function editProjectCoverRequest(values) {
  return {
    type: EDIT_PROJECT_COVER_REQUEST,
    values: values
  };
}

export function editProjectCoverSuccess(data) {
  return {
    type: EDIT_PROJECT_COVER_SUCCESS,
    data: data
  };
}

export const POST_PROJECT_INITIAL = "POST_PROJECT_INITIAL";
export const POST_PROJECT_REQUEST = "POST_PROJECT_REQUEST";
export const POST_PROJECT_SUCCESS = "POST_PROJECT_SUCCESS";
export const POST_PROJECT_FAILED = "POST_PROJECT_FAILED";

export function postProjectInitial() {
  return {
    type: POST_PROJECT_INITIAL
  };
}

export function postProjectRequest(values) {
  return {
    type: POST_PROJECT_REQUEST,
    values: values
  };
}

export function postProjectSuccess(data) {
  return {
    type: POST_PROJECT_SUCCESS,
    data: data
  };
}

export function postProjectFailed(error) {
  return {
    type: POST_PROJECT_FAILED,
    error: error
  };
}

export const POST_PROJECT_COVER_INITIAL = "POST_PROJECT_COVER_INITIAL";
export const POST_PROJECT_COVER_REQUEST = "POST_PROJECT_COVER_REQUEST";
export const POST_PROJECT_COVER_SUCCESS = "POST_PROJECT_COVER_SUCCESS";
export const POST_PROJECT_COVER_FAILED = "POST_PROJECT_COVER_FAILED";

export function postProjectCoverInitial() {
  return {
    type: POST_PROJECT_COVER_INITIAL
  };
}

export function postProjectCoverRequest(values) {
  return {
    type: POST_PROJECT_COVER_REQUEST,
    values: values
  };
}

export function postProjectCoverSuccess(data) {
  return {
    type: POST_PROJECT_COVER_SUCCESS,
    data: data
  };
}

export function postProjectCoverFailed(error) {
  return {
    type: POST_PROJECT_COVER_FAILED,
    error: error
  };
}

export const POST_PROJECT_COVER_ORIGINAL_INITIAL = "POST_PROJECT_COVER_ORIGINAL_INITIAL";
export const POST_PROJECT_COVER_ORIGINAL_REQUEST = "POST_PROJECT_COVER_ORIGINAL_REQUEST";
export const POST_PROJECT_COVER_ORIGINAL_SUCCESS = "POST_PROJECT_COVER_ORIGINAL_SUCCESS";
export const POST_PROJECT_COVER_ORIGINAL_FAILED = "POST_PROJECT_COVER_ORIGINAL_FAILED";

export function postProjectCoverOriginalInitial() {
  return {
    type: POST_PROJECT_COVER_ORIGINAL_INITIAL
  };
}

export function postProjectCoverOriginalRequest(values) {
  return {
    type: POST_PROJECT_COVER_ORIGINAL_REQUEST,
    values: values
  };
}

export function postProjectCoverOriginalSuccess(data) {
  return {
    type: POST_PROJECT_COVER_ORIGINAL_SUCCESS,
    data: data
  };
}

export function postProjectCoverOriginalFailed(error) {
  return {
    type: POST_PROJECT_COVER_ORIGINAL_FAILED,
    error: error
  };
}