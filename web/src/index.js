import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";
import { routerMiddleware } from "react-router-redux";
import { createStore, combineReducers, applyMiddleware } from "redux";
import * as createHistory from "history";
import { addLocaleData } from "react-intl";
import en from "react-intl/locale-data/en";
import createSagaMiddleware from "redux-saga";

import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

import reducers from "./services/reducers";
import sagas from "./services/sagas";

const sagaMiddleware = createSagaMiddleware();
const history = createHistory.createBrowserHistory();

const store = createStore(
  combineReducers({
    ...reducers
  }),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  applyMiddleware(sagaMiddleware, routerMiddleware(history))
);

addLocaleData([...en]);
sagaMiddleware.run(sagas);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
