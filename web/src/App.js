import React, { Component } from "react";
import Loadable from "react-loadable";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { IntlProvider } from "react-intl";

import Loading from "./components/Loading";
import enMessages from "./services/locales/en";

import "./App.css";

const Home = Loadable({
  loader: () => import("./scenes/Home"),
  loading: Loading
});
const Login = Loadable({
  loader: () => import("./scenes/LoginPage"),
  loading: Loading
});
const Register = Loadable({
  loader: () => import("./scenes/RegisterPage"),
  loading: Loading
});
const Profile = Loadable({
  loader: () => import("./scenes/ProfilePage"),
  loading: Loading
});
const ProjectSteps = Loadable({
  loader: () => import("./scenes/ProjectStepsPage"),
  loading: Loading
});

class App extends Component {
  render() {
    const { language } = this.props;
    const messages = { en: enMessages };
    return (
      <IntlProvider
        locale={language.country}
        messages={messages[language.code]}
      >
        <Router>
          <Switch>
            <Route path="/" exact component={Home}/>
            <Route path="/login" exact component={Login}/>
            <Route path="/register" exact component={Register}/>
            <Route path="/profile" exact component={Profile}/>
            <Route path="/project" exact component={ProjectSteps}/>
          </Switch>
        </Router>
      </IntlProvider>
    );
  }
}

App.defaultProps = {
  language: {
    code: "en",
    country: "en-US"
  }
};

export default App;
