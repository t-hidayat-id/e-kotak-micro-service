import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from "react-router-dom";
import { Form,  Input, Checkbox, Button } from 'antd';
import styled from 'styled-components';
import * as actions from "../services/actions";

import logo from '../assets/e-kotak.svg';

const StyledContainer = styled.div`
  width: 500px;
  left: 50%;
  margin-left: -250px;
  position: relative;
  margin-top: 5%;
  box-sizing: border-box;
`;
const StyledLogoText = styled.div`
  width: 100%;
  font-size: 30px;
  margin-bottom: 20px;
  color: rgb(30, 30, 30);
`;
const StyledSloganText = styled.div`
  width: 100%;
  font-size: 20px;
  margin-top: -30px;
  position: absolute;
  color: rgb(132, 132, 132);
`;
const StyledForm = styled(Form)`
  width: 500px;
`;
const Logo = styled.div`
  height: 40px;
  img {
    height: 40px;
  }
`;

class Register extends Component {
  state = {
    confirmDirty: false,
    error: null, 
    success: false 
  };

  componentDidUpdate(prevProps, prevState) {
    if (this.props.register.success === true && this.props !== prevProps) {
      this.props.history.push('/login');
    }
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        this.props.dispatch(actions.registerRequest(values));
      }
    });
  };

  handleConfirmBlur = e => {
    const { value } = e.target;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };

  compareToFirstPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && value !== form.getFieldValue('password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 16,
          offset: 8,
        },
      },
    };

    return (
      <StyledContainer>
        <StyledForm {...formItemLayout} onSubmit={this.handleSubmit}>
          <div className="ant-row ant-form-item">
            <div className="ant-col ant-form-item-label ant-col-xs-24 ant-col-sm-8"/>
            <div className="ant-col ant-form-item-control-wrapper ant-col-xs-24 ant-col-sm-16">
              <Logo>
                <img src={logo} alt="logo"/>
              </Logo>
              <StyledLogoText>Sign Up</StyledLogoText>
              <StyledSloganText>to continue using e-kotak</StyledSloganText>
            </div>
          </div>
          <Form.Item label="First Name">
            {getFieldDecorator('firstName', {
              rules: [
                {
                  required: true,
                  message: 'Please input your first name!',
                },
              ],
            })(<Input />)}
          </Form.Item>
          <Form.Item label="Last Name">
            {getFieldDecorator('lastName', {
              rules: [
                {
                  required: true,
                  message: 'Please input your last name!',
                },
              ],
            })(<Input />)}
          </Form.Item>
          <Form.Item label="E-mail">
            {getFieldDecorator('email', {
              rules: [
                {
                  type: 'email',
                  message: 'The input is not valid E-mail!',
                },
                {
                  required: true,
                  message: 'Please input your E-mail!',
                },
              ],
            })(<Input />)}
          </Form.Item>
          <Form.Item label="Password" hasFeedback>
            {getFieldDecorator('password', {
              rules: [
                {
                  required: true,
                  message: 'Please input your password!',
                },
                {
                  validator: this.validateToNextPassword,
                },
              ],
            })(<Input.Password />)}
          </Form.Item>
          <Form.Item label="Confirm Password" hasFeedback>
            {getFieldDecorator('confirm', {
              rules: [
                {
                  required: true,
                  message: 'Please confirm your password!',
                },
                {
                  validator: this.compareToFirstPassword,
                },
              ],
            })(<Input.Password onBlur={this.handleConfirmBlur} />)}
          </Form.Item>
          <Form.Item {...tailFormItemLayout}>
            {getFieldDecorator('agreement', {
              valuePropName: 'checked',
            })(
              <Checkbox>
                I have read the <a href="/">agreement</a>
              </Checkbox>,
            )}
          </Form.Item>
          <Form.Item {...tailFormItemLayout}>
            <Button type="primary" htmlType="submit">
              Register
            </Button>
          </Form.Item>
        </StyledForm>
      </StyledContainer>
    );
  }
}

const mapStateToProps = (state, ) => {
  return {
    register: state.register,
  }
};

export default  connect(mapStateToProps)(
  Form.create({ name: 'register' })(withRouter(Register))
);