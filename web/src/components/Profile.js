import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from "react-router-dom";
import get from 'lodash/get';
import { Form,  Input, Button, Icon } from 'antd';
import styled from 'styled-components';
import slugify from 'slugify';

import * as actions from "../services/actions";
import logo from '../assets/e-kotak.svg';

const { TextArea } = Input;

const StyledContainer = styled.div`
  width: 500px;
  left: 50%;
  margin-left: -250px;
  position: relative;
  margin-top: 5%;
  box-sizing: border-box;
`;
const StyledLogoText = styled.div`
  width: 100%;
  font-size: 30px;
  margin-bottom: 20px;
  color: rgb(30, 30, 30);
`;
const StyledSloganText = styled.div`
  width: 100%;
  font-size: 20px;
  margin-top: -30px;
  position: absolute;
  color: rgb(132, 132, 132);
`;
const StyledForm = styled(Form)`
  width: 500px;
`;
const Logo = styled.div`
  height: 40px;
  img {
    height: 40px;
  }
`;

function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}

class Profile extends Component {
  state = {
    confirmDirty: false,
    error: null, 
    success: false,
    linkId: null
  };

  componentDidMount() {
    const id = localStorage.getItem("id");
    if (id) {
      this.props.dispatch(actions.getProfileRequest());
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if ((this.props.postProfile.success === true || this.props.putProfile.success === true)  && this.props !== prevProps) {
      if (this.props.getProfile.success === false) {
        this.props.dispatch(actions.getProfileRequest());
      }
    }
    if (this.props.getProfile.success === true && this.props !== prevProps) {
      this.setState(
        {
          linkId: get(this.props.getProfile.data, 'linkId')
        }
      )
    }
    if ((this.props.postProfile.error.status || this.props.putProfile.error.status) && this.props !== prevProps) {
      const errors = this.props.postProfile.error.status ? this.props.postProfile.error.result.errors : this.props.putProfile.error.result.errors;
      if (get(errors, 'linkId')) {
        this.setState({linkId: this.props.form.getFieldValue('linkId')});
        this.props.form.setFields({
          linkId: {
            errors: [new Error(get(errors, 'linkId'))],
          }
        }); 
      }
      this.props.dispatch(actions.postProfileInitial());
      this.props.dispatch(actions.putProfileInitial());
    }
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        if (!this.props.getProfile.success) {
          this.props.dispatch(actions.postProfileRequest(values));
        } else {
          this.props.dispatch(actions.putProfileRequest(values));
        }
        this.props.dispatch(actions.getProfileInitial());
      }
    });
  };

  handleConfirmBlur = e => {
    const { value } = e.target;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };

  validateDisplayName = (rule, value, callback) => {
    if (get(this.props.getProfile.data, 'displayName') === undefined) {
      this.setState({linkId: slugify(value, {lower: true})});
      if (this.props.form.getFieldError('linkId')) {
        this.props.form.setFields({
          linkId: {
            value: this.props.form.getFieldValue('linkId'),
            errors: null,
          }
        }); 
      }
    }
    callback();
  };

  onLinkIdChange = e => {
    if (e.target.value === '') {
      this.props.form.resetFields();
    }
  }

  render() {
    const { 
      getFieldDecorator, 
      getFieldsError, 
      getFieldError, 
      isFieldTouched 
    } = this.props.form;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 16,
          offset: 8,
        },
      },
    };

    const displayNameError = isFieldTouched(
      'displayName'
    ) && getFieldError('displayName');
    const linkIdError = (isFieldTouched(
      'linkId'
    ) && getFieldError('linkId')) || getFieldError('linkId');
    const disableSubmit = (
      isFieldTouched('displayName') === undefined && 
      isFieldTouched('linkId') === undefined 
    );

    return (
      <StyledContainer>
        <StyledForm {...formItemLayout} onSubmit={this.handleSubmit}>
          <div className="ant-row ant-form-item">
            <div className="ant-col ant-form-item-label ant-col-xs-24 ant-col-sm-8"/>
            <div className="ant-col ant-form-item-control-wrapper ant-col-xs-24 ant-col-sm-16">
              <Logo>
                <img src={logo} alt="logo"/>
              </Logo>
              <StyledLogoText>Public Profile</StyledLogoText>
              <StyledSloganText>for make a project or a donation.</StyledSloganText>
            </div>
          </div>
          <Form.Item 
            label="Display Name" 
            extra="This is the public name will be display on you project or donation."
            validateStatus={displayNameError ? 'error' : isFieldTouched('displayName') ? 'success' : ''}
            help={displayNameError || ''}
            hasFeedback
          >
            {getFieldDecorator('displayName', {
              initialValue: get(this.props.getProfile.data, 'displayName'),
              rules: [
                {
                  required: true,
                  message: 'Display name is required!',
                },
                {
                  validator: this.validateDisplayName,
                },
                {
                  min: 10,
                  max: 50,
                  message: "Display name must be between 10 and 50 characters!"
                },
              ],
            })(<Input onChange={this.onDisplayNameChange} />)}
          </Form.Item>
          <Form.Item 
            label="Link Profile"
            validateStatus={linkIdError ? 'error' : ''}
            help={linkIdError || ''}
          >
            {getFieldDecorator('linkId', {
              initialValue: this.state.linkId,
              rules: [
                {
                  required: true,
                  message: 'Link profile is required!',
                },
                {
                  min: 10,
                  max: 50,
                  message: "Link profile must be between 10 and 50 characters!"
                },
              ],
            })(<Input 
                  addonBefore="www.e-kotak.com/"  
                  suffix={
                    <Icon 
                      type="close-circle" 
                      style={{color: 'rgba(0,0,0,.25)'}}
                      onClick={e => {this.props.form.resetFields(['linkId'])}}
                    />
                  } 
               />
            )}
          </Form.Item>
          <Form.Item 
            label="Description" 
          >
            {getFieldDecorator('description', {
              initialValue: get(this.props.getProfile.data, 'description'),
              rules: [
                {
                  required: false
                },
                {
                  min: 30,
                  max: 100,
                  message: "Description must be between 30 and 100 characters!"
                },
              ],
            })(<TextArea autosize={{ minRows: 4, maxRows: 6 }}/>)}
          </Form.Item>
          <Form.Item {...tailFormItemLayout}>
            <Button 
              type="primary" 
              htmlType="submit"
              className="login-form-button"
              loading={
                this.props.postProfile.request || 
                this.props.getProfile.request ||
                this.props.putProfile.request ? true : false
              }
              disabled={hasErrors(getFieldsError()) || disableSubmit}
            >
              Save
            </Button>
          </Form.Item>
        </StyledForm>
      </StyledContainer>
    );
  }
}

const mapStateToProps = (state, ) => {
  return {
    postProfile: state.postProfile,
    getProfile: state.getProfile,
    putProfile: state.putProfile
  }
};

export default  connect(mapStateToProps)(
  Form.create({ name: 'profile' })(withRouter(Profile))
);