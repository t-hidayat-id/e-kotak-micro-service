import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from "react-router-dom";
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import styled from 'styled-components';
import * as actions from "../services/actions";

import logo from '../assets/e-kotak.svg';

const StyledContainer = styled.div`
  max-width: 300px;
  left: 50%;
  margin-left: -150px;
  position: relative;
  margin-top: 10%;
  box-sizing: border-box;
`;
const StyledLogoText = styled.div`
  width: 100%;
  font-size: 30px;
  text-align: center;
  margin-bottom: 30px;
  color: rgb(30, 30, 30);
`;
const StyledSloganText = styled.div`
  width: 100%;
  font-size: 20px;
  text-align: center;
  margin-top: -40px;
  position: absolute;
  color: rgb(132, 132, 132);
`;
const StyledForm = styled(Form)`
  .login-form-forgot {
    float: right;
  };
  .login-form-button {
    width: 100%;
  };
`;
const Logo = styled.div`
  height: 40px;
  img {
    height: 40px;
  }
  left: 50%;
  position: relative;
  margin-left: -20px;
`;
const RegisterLink = styled.span`
  color: #1890ff;
  cursor: pointer;
`;


class Login extends Component {
  constructor(props) {
    super(props);
    this.state = { error: null, success: false };
  }

  componentDidMount() {}

  componentDidUpdate(prevProps, prevState) {
    if (this.props.login.success === true && this.props !== prevProps) {
      this.props.dispatch(actions.userRequest());
    }
    if (this.props.user.success === true && this.props !== prevProps) {
      this.props.history.push('/');
    }
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.dispatch(actions.loginRequest(values));
      } else {
        this.setState({error: {status: 400}});
      }
    });
  };

  registerLinkClick = () => {
    this.props.history.push('/register');
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <StyledContainer>
        <Logo>
          <img src={logo} alt="logo"/>
        </Logo>
        <StyledLogoText>Sign In</StyledLogoText>
        <StyledSloganText>to continue using e-kotak</StyledSloganText>
        <StyledForm onSubmit={this.handleSubmit} className="login-form">
          <Form.Item>
            {getFieldDecorator('email', {
              rules: [{ required: true, message: 'Please input your email!' }],
            })(
              <Input
                prefix={
                  <Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }}/>
                }
                placeholder="Email"
              />,
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('password', {
              rules: [{ 
                required: true, 
                message: 'Please input your Password!'
              }],
            })(
              <Input
                prefix={
                  <Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }}/>
                }
                type="password"
                placeholder="Password"
              />,
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('remember', {
              valuePropName: 'checked',
              initialValue: true,
            })(<Checkbox>Remember me</Checkbox>)}
            <a className="login-form-forgot" href="http://google.com">
              Forgot password
            </a>
            <Button 
              type="primary" 
              htmlType="submit" 
              className="login-form-button"
              loading={
                this.props.login.request || this.props.user.request ? 
                  true : false
              }
            >
              Log in
            </Button>
            Or <RegisterLink onClick={this.registerLinkClick}>
              register now!
            </RegisterLink>
          </Form.Item>
        </StyledForm>
      </StyledContainer>
    );
  }
}

const mapStateToProps = (state, ) => {
  return {
    login: state.login,
    user: state.user
  }
};

export default  connect(mapStateToProps)(
  Form.create({ name: 'normal_login' })(withRouter(Login))
);