import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { Slider, Upload, Button } from 'antd';
import AvatarEditor from 'react-avatar-editor';

import * as actions from "../services/actions";

const StyledContainer = styled.div`
  max-width: 100%;
`;

class ProjectCover extends Component {
  state = {
    scale: 1.2,
    file: null,
    fileList: [],
    position: null,
    encodedImage: null,
  };

  componentDidMount() {
    const { data } = this.props.editProjectCover;
    if (data) {
      const { scale, file, fileList, position } = data;
      this.setState({
        scale: scale, file: file, fileList: fileList, position: position
      })
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state !== prevState) {
      this.props.dispatch(actions.editProjectCoverRequest(this.state));
    }
  }

  handleScale = (e) => {
    const scale = parseFloat(e);
    this.setState({scale: scale});
  };
  
  beforeUpload = (file) => {
    this.setState({file: file});
    return false;
  };

  handleChange = info => {
    let fileList = [...info.fileList];
    fileList = fileList.slice(-1);
    fileList = fileList.map(file => {
      if (file.response) {
        file.url = file.response.url;
      }
      return file;
    });
    console.log(fileList);
    this.setState({ fileList: fileList });
  };

  handlePositionChange = (position) => {
    const encodedImage = this.editor.getImageScaledToCanvas().toDataURL("image/jpeg");
    this.setState({
      position: position,
      encodedImage: encodedImage
    });
  }

  handleUpload = ()  => {
    this.props.dispatch(actions.postProjectCoverOriginalRequest( {
      projectId: this.props.postProject.data.projectId,
      file:  this.state.file
    }));
    this.props.dispatch(actions.postProjectCoverRequest( {
      projectId: this.props.postProject.data.projectId,
      encodedImage:  this.state.encodedImage,
      properties: JSON.stringify({
        scale: this.state.scale,
        fileList: this.state.fileList,
        position: this.state.position
      })
    }));
  }

  setEditorRef = (editor) => this.editor = editor;

  render() {
    const { file, position, scale, fileList} = this.state;
    const props = {onChange: this.handleChange};  
    return (
      <StyledContainer>
        <AvatarEditor
          ref={this.setEditorRef}
          image={file}
          width={900}
          height={400}
          border={40}
          color={[255, 255, 255, 0.6]}
          scale={scale}
          rotate={0}
          style={{width: '100%', height: 400}}
          position={position}
          onPositionChange={this.handlePositionChange}
        />
        <Slider 
          min={1} 
          max={2} 
          step={0.01} 
          value={this.state.scale} 
          onChange={this.handleScale}
        />
        <Upload 
          {...props}
          fileList={fileList} 
          beforeUpload={this.beforeUpload.bind(this)}
        >
          <Button type="primary" shape="circle" icon="upload"/>
        </Upload>
        <Button type="primary" style={{marginTop: '10px'}} onClick={this.handleUpload}>
          Save
        </Button>
      </StyledContainer>
    )   
  }
}

const mapStateToProps = (state, ) => {
  return {
    editProject: state.editProject,
    editProjectCover: state.editProjectCover,
    postProject: state.postProject,
    postProjectCover: state.postProjectCover,
    postProjectCoverOriginal: state.postProjectCoverOriginal
  }
};


export default connect(mapStateToProps)(ProjectCover);