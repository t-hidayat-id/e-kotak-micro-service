import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from "react-router-dom";
import { Form, Select, Switch, Icon, Button } from 'antd';
import get from 'lodash/get';

const { Option } = Select;

class ProjectPublish extends Component {
     render() {
        const { getFieldDecorator } = this.props.form;  
         return(
            <Form layout="vertical">
                <Form.Item label="Category">
                    {getFieldDecorator('category', {
                        initialValue: get(this.props.editProject.data, 'category'),
                        rules: [
                            { 
                                required: true, 
                                message: 'Please select your category!' 
                            }
                        ],
                    })(
                        <Select defaultValue="lucy" style={{ width: 120 }}>
                            <Option value="jack">Jack</Option>
                            <Option value="lucy">Lucy</Option>
                        </Select>
                    )}
                </Form.Item>
                <Form.Item label="Online">
                    {getFieldDecorator('online', {
                        initialValue: get(this.props.editProject.data, 'online'),
                    })(
                        <Switch
                            checkedChildren={<Icon type="check" />}
                            unCheckedChildren={<Icon type="close" />}        
                        />
                    )}
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit">
                        Save
                    </Button>
                </Form.Item>
            </Form>
         )
     }
}

const mapStateToProps = (state, ) => {
    return {
        getProfile: state.getProfile,
        editProject: state.editProject,
        postProject: state.postProject
    }
  };

export default  connect(mapStateToProps)(
    Form.create({ name: 'projectPublish' })(withRouter(ProjectPublish))
  );