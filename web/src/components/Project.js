import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from "react-router-dom";
import { Form, Input, Button } from 'antd';
import get from 'lodash/get';
import slugify from 'slugify';

import * as actions from "../services/actions";
import RichTextWrapper from './RichTextWrapper';

class Project extends Component {
  componentDidMount() {
    const id = localStorage.getItem("id");
    if (id) {
      this.props.dispatch(actions.getProfileRequest());
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.editProject.success === true && this.props.editProject !== prevProps.editProject) {
      this.props.form.setFieldsValue({
        'description': get(this.props.editProject.data, 'description.document.text')
      });
    }
  }
  onTitleChange = (e) => {
    const { value } = e.target;
    this.props.dispatch(actions.editProjectRequest(
      {
        "title": value,
        "description":  get(this.props.editProject.data, 'description'),
        "slug": slugify(value.toLowerCase())
      }
    ));
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        const data = this.props.editProject.data;
        this.props.dispatch(actions.postProjectRequest(
          {
            'title': data.title,
            'slug': data.slug,
            'description': JSON.stringify(data.description.toJSON())
          }
        ));
      } 
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;  
    const linkId = get(this.props.getProfile.data, 'linkId');
    const slug = get(this.props.editProject.data, 'slug');
    return (
      <Form layout="vertical" onSubmit={this.handleSubmit}>
        <Form.Item label="Title">
          {getFieldDecorator('title', {
            initialValue: get(this.props.editProject.data, 'title'),
            rules: [
              { 
                required: true, 
                message: 'Please input the title of project!' 
              }
           ],
          })(<Input autoFocus placeholder="Title of your project ..." onChange={this.onTitleChange} />)}
          <b>https://www.e-kotak.com/{linkId}/{slug}</b>
        </Form.Item>
        <Form.Item label="Description">
          {getFieldDecorator('description', {
            initialValue: get(this.props.editProject.data, 'description.document.text'),
            rules: [
              { 
                required: true, 
                message: 'Please input the description of project!' 
              }
           ],
          })(<RichTextWrapper />)}
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            Save
          </Button>
        </Form.Item>
      </Form>
    )     
  }
}

const mapStateToProps = (state, ) => {
  return {
    getProfile: state.getProfile,
    editProject: state.editProject,
    postProject: state.postProject
  }
};

export default  connect(mapStateToProps)(
  Form.create({ name: 'project' })(withRouter(Project))
);