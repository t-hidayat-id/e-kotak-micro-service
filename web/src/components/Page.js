import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from "react-router-dom";
import { Layout, Menu, Icon, AutoComplete, Input, Badge } from 'antd';
import styled from 'styled-components';

import * as actions from "../services/actions";
import logo from '../assets/e-kotak-white.svg';

const { Header, Content, Footer, Sider } = Layout;

const Logo = styled.div`
  height: 40px;
  margin: 16px;
  cursor: pointer;
  img {
    height: 40px;
  }
`;
const IconTrigger = styled(Icon)`
  font-size: 18px;
  line-height: 64px;
  padding: 0 24px;
  cursor: pointer;
  transition: color 0.3s;
  :hover {
    color: #1890ff;
  }
`;
const AutoCompletedStyled = styled(AutoComplete)`
  margin-left: 10px;
  display: inline-block;
  vertical-align: middle;
  height: 32px;
`;
const HeaderStyled = styled(Header)`
  background: #fff;
  padding: 0;
`;
const ContentStyled = styled(Content)`
  margin: 24px 16px 0;
`;
const FooterStyled = styled(Footer)`
  text-align: center;
`
const MenuHeader = styled(Menu)`
  line-height: 64px;
  float: right;
  margin-right: 16px;
`;
const BadgeNotification = styled(Badge)`
  margin-top: -5px;
  margin-right: 15px;
  cursor: pointer;
`;
const IconNofication = styled(Icon)`
  marginRight: 15px; 
  cursor: pointer;
`;
const SiderStyled = styled(Sider)`
  height: 100vh;
`;

class Page extends Component {
  state = {
    collapsed: true,
  };

  componentDidMount() {
    const id = localStorage.getItem("id");
    if (id) {
      this.props.dispatch(actions.userRequest());
    }
  }

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  logoClick = () => {
    this.props.history.push('/');
  }

  render() {
    return (
      <Layout>
        <SiderStyled
          breakpoint="sm"
          onBreakpoint={broken => {
            // console.log(broken);
          }}
          onCollapse={(collapsed, type) => {
            // console.log(collapsed, type);
          }}
          trigger={null}
          collapsible
          collapsed={this.state.collapsed}
        >
          <Logo onClick={this.logoClick}>
            <img src={logo} alt="logo" />
          </Logo>
          <Menu theme="dark" mode="inline"
            defaultSelectedKeys={
              this.props.defaultMenuSiderKey
                ? this.props.defaultMenuSiderKey : ['home']
            }
            onClick={this.props.menuSiderClick}
          >
            <Menu.Item key="home">
              <Icon type="home" />
              <span className="nav-text">Home</span>
            </Menu.Item>
            {this.props.menuSider}
            <Menu.Item key="help">
              <Icon type="question-circle-o" />
              <span className="nav-text">Help</span>
            </Menu.Item>
          </Menu>
        </SiderStyled>
        <Layout>
          <HeaderStyled>
            <IconTrigger
              className="trigger"
              type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
              onClick={this.toggle}
            />
            <AutoCompletedStyled
              className="search"
              dropdownMatchSelectWidth={false}
              style={{ width: '35%' }}
              placeholder="Search ..."
              optionLabelProp="value">
              <Input suffix={
                <Icon type="search" className="certain-category-icon" />
              } />
            </AutoCompletedStyled>
            <MenuHeader
              theme="light"
              mode="horizontal"
              onClick={this.props.menuHeaderClick}
            >
              <Menu.Item key="notification">
                <BadgeNotification count={5}>
                  <IconNofication type="notification" />
                </BadgeNotification>
              </Menu.Item>
              {this.props.menuHeader}
            </MenuHeader>
          </HeaderStyled>
          <ContentStyled>
            {this.props.content}
          </ContentStyled>
          <FooterStyled>
            Copyright ©2019 e-kotak. All right reserved.
          </FooterStyled>
        </Layout>
      </Layout>
    );
  }
}

const mapStateToProps = (state, ) => {
  return {
    user: state.user
  }
};

export default connect(mapStateToProps)(withRouter(Page));