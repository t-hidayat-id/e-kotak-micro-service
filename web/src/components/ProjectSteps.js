import React, { Component } from 'react';
import { Steps, Icon } from 'antd';
import styled from 'styled-components';

import Project from './Project';
import ProjectCover from './ProjectCover';
import ProjectPublish from './ProjectPublish';

const { Step } = Steps;

const stepStyle = {
  marginBottom: 20,
  boxShadow: '0px -1px 0 0 #e8e8e8 inset',
};

const StyledContainer = styled.div`
  max-width: 60%;
  left: 50%;
  margin-left: -30%;
  position: relative;
  box-sizing: border-box;
`;

class ProjectSteps extends Component {
  state = {
    current: 0,
  };

  onChange = current => {
    this.setState({ current });
  };

  onEditorChange = ({ value }) => {
    this.setState({ value })
  };

  render() {
    const { current } = this.state;
    return (
      <StyledContainer>
        <Steps 
          type="navigation"
          size="small"
          current={current}
          onChange={this.onChange}
          style={stepStyle}
        >
          <Step title="Detail" status="finish" icon={<Icon type="edit" />}/>
          <Step title="Photo" status="finish" icon={<Icon type="pic-center" />}/>
          <Step title="Publish" status="finish" icon={<Icon type="check" />}/>
        </Steps>
        {current === 0 && <Project />}
        {current === 1 && <ProjectCover/>}
        {current === 2 && <ProjectPublish/>}
      </StyledContainer>
    )
  }
}

export default ProjectSteps;