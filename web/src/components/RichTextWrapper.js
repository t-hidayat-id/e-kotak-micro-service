import React, { Component } from 'react';

import RichText from './RichText';

export default class RichTextWrapper extends Component {
    render() {
      const {children, ...props} = this.props
      return <RichText {...props}>{children}</RichText>
    }
  };