import React from "react";
import { Spin } from "antd";
import { notification } from "antd";

const Loading = props => {
  const openNotification = type => {
    const args = {
      message: "Failed!",
      description: "Something happen there, please refresh your browser.",
      duration: 0
    };
    notification[type](args);
    return null;
  };
  const spin = () => {
    return (
      <Spin
        size="large"
        style={{
          position: "fixed",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)"
        }}
      />
    );
  };
  if (props.isLoading) {
    if (props.timedOut) {
      return openNotification("error");
    } else {
      return spin();
    }
  } else if (props.error) {
    return openNotification("error");
  } else {
    return null;
  }
};

export default Loading;
