package com.ekotak.project.controllers;

import java.util.UUID;

import javax.validation.Valid;

import com.ekotak.project.domain.Project;
import com.ekotak.project.models.ProjectPostModel;
import com.ekotak.project.models.ProjectPutModel;
import com.ekotak.project.services.ProjectByProfileService;
import com.ekotak.project.services.ProjectService;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/projects")
public class ProjectController {

    @Autowired
    ProjectService projectService;

    @Autowired
    ProjectByProfileService projectByProfileService;

    @PostMapping(
        consumes = { 
            MediaType.APPLICATION_XML_VALUE, 
            MediaType.APPLICATION_JSON_VALUE 
        },
		produces = { 
            MediaType.APPLICATION_XML_VALUE, 
            MediaType.APPLICATION_JSON_VALUE 
        }
	)
    public ResponseEntity<Object> create(
        @Valid @RequestBody ProjectPostModel projectPostModel) 
    {
        ModelMapper modelMapper = new ModelMapper();
        Project project = modelMapper.map(projectPostModel, Project.class);
        
        Project createProject = this.projectService.create(project);
        return ResponseEntity.status(HttpStatus.CREATED).body(createProject);
    }

    @GetMapping(
        consumes = { 
            MediaType.APPLICATION_XML_VALUE, 
            MediaType.APPLICATION_JSON_VALUE 
        }
    )
    public ResponseEntity<Object> findAllByProfile() {
        return ResponseEntity.status(HttpStatus.OK).body(
            projectByProfileService.findAllByProfile()
        );
    }

    @GetMapping(
        value="/{projectId}", 
        consumes = { 
            MediaType.APPLICATION_XML_VALUE, 
            MediaType.APPLICATION_JSON_VALUE 
        }
    )
    public ResponseEntity<Object> 
        findByProjectId(@PathVariable("projectId") UUID projectId)
    {
        Project project = projectService.findByProjectId(projectId); 
        return ResponseEntity.status(HttpStatus.OK).body(project);
    }

    @PutMapping(
        consumes = { 
            MediaType.APPLICATION_XML_VALUE, 
            MediaType.APPLICATION_JSON_VALUE 
        },
		produces = { 
            MediaType.APPLICATION_XML_VALUE, 
            MediaType.APPLICATION_JSON_VALUE 
        }
	)
    public ResponseEntity<Object> update(
        @Valid @RequestBody ProjectPutModel projectPutModel) 
    {
        ModelMapper modelMapper = new ModelMapper();
        Project project = modelMapper.map(projectPutModel, Project.class);
        
        Project udpateProject = this.projectService.update(project);
        return ResponseEntity.status(HttpStatus.OK).body(udpateProject);
    }
    
}
