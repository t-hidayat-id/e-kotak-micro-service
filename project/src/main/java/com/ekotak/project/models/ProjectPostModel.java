package com.ekotak.project.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.ekotak.project.validators.IsNewSlug;

import lombok.Data;
import lombok.Generated;

@Data
public class ProjectPostModel {

    @NotNull(message = "Title cannot be null")
    @Size(
        min=15, 
        max=50,
        message="Title must be equal or greater than 15 characters" + 
            " and less than 50 characters"
    )
    @Generated
    private String title;

    @Size(
        min=10,
        message="Slug must be equal or greater than 10 characters" 
    )
    @IsNewSlug
    private String slug;

    @NotNull(message="Description cannot be null")
    @Size(
        min=50, 
        message="Descrition must be greater than 50 characters" 
    )
    private String description;

}