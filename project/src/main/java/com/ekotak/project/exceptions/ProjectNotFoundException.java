package com.ekotak.project.exceptions;

import java.util.UUID;

public class ProjectNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 9046693557485355854L;

    private UUID projectId;

    public ProjectNotFoundException() {

    }

    public ProjectNotFoundException(UUID projectId) {
        super("Project is not found.");
        
        this.projectId = projectId;
    }

    public UUID getProjectId() {
        return this.projectId;
    }

}