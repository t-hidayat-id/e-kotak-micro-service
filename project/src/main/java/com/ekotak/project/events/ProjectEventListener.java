package com.ekotak.project.events;

import com.ekotak.project.domain.Project;
import com.ekotak.project.domain.ProjectByProfile;
import com.ekotak.project.domain.ProjectByProfileKey;
import com.ekotak.project.repositories.ProjectByProfileRepository;
import com.ekotak.project.repositories.ProjectRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.core.mapping.event.AbstractCassandraEventListener;
import org.springframework.data.cassandra.core.mapping.event.BeforeSaveEvent;

public class ProjectEventListener extends AbstractCassandraEventListener<Project> {

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    ProjectByProfileRepository projectByProfileRepository;

    /**
     * Save or update a project by profile.
     * Delete an old project by profile if the new slug is different 
     * with a new one.
     */
    @Override
    public void onBeforeSave(BeforeSaveEvent<Project> event) {
        Project project = event.getSource();

        ModelMapper modelMapper = new ModelMapper();
        ProjectByProfile projectByProfile = modelMapper.map(
            project, ProjectByProfile.class
        );

        ProjectByProfileKey key = new ProjectByProfileKey(
            project.getProfileId(), project.getSlug()
        );
        projectByProfile.setKey(key);
        
        Project oldProject = projectRepository.findByProjectId(
            project.getProjectId()
        ).orElse(null);
        if (oldProject != null) {
            if (!oldProject.getSlug().equals(project.getSlug())) {
                ProjectByProfile oldProjectByProfile = 
                    projectByProfileRepository.findByKeyProfileIdAndKeySlug(
                        oldProject.getProfileId(), oldProject.getSlug()
                    ).orElse(null);
                
                if (oldProjectByProfile != null) {
                    projectByProfileRepository.delete(oldProjectByProfile);
                }
            }
        }

        projectByProfileRepository.save(projectByProfile);
	}
    
}