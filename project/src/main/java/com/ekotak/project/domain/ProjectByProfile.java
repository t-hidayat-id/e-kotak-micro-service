package com.ekotak.project.domain;

import java.io.Serializable;
import java.util.UUID;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import lombok.Data;

@Data
@Table("project_by_profile")
public class ProjectByProfile implements Serializable {

    private static final long serialVersionUID = 7823551093374144890L;

    @PrimaryKey
    ProjectByProfileKey key;
    
    @Column("project_id")
    UUID projectId;

    @Column("title")
    String title;

    @Column("description")
    String description;

    public ProjectByProfile() {}

    public ProjectByProfile(String title, String description) {
        this.title = title;
        this.description = description;
    }

}