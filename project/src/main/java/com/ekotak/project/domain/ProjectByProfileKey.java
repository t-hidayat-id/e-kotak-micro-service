package com.ekotak.project.domain;

import java.io.Serializable;
import java.util.UUID;

import org.springframework.data.cassandra.core.cql.Ordering;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

import lombok.Data;

@Data
@PrimaryKeyClass
public class ProjectByProfileKey implements Serializable {

    private static final long serialVersionUID = 6080028239618069781L;

    @PrimaryKeyColumn(
        name = "profile_id", 
        ordinal = 0, 
        type = PrimaryKeyType.PARTITIONED
    )
    private UUID profileId;

    @PrimaryKeyColumn(
        name = "slug", 
        ordinal = 1, 
        type = PrimaryKeyType.CLUSTERED,
        ordering = Ordering.DESCENDING
    )
    private String slug;
    
    public ProjectByProfileKey(UUID profileId, String slug) {
        this.profileId = profileId;
        this.slug = slug;
    }
}