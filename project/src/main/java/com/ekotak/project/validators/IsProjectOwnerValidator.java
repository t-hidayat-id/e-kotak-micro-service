package com.ekotak.project.validators;

import java.util.UUID;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.ekotak.project.domain.Profile;
import com.ekotak.project.domain.Project;
import com.ekotak.project.repositories.ProjectRepository;
import com.ekotak.project.services.ProfileService;

import org.springframework.beans.factory.annotation.Autowired;

public class IsProjectOwnerValidator 
    implements ConstraintValidator<IsProjectOwner, UUID> 
{

    @Autowired
    ProfileService profileService;

    @Autowired
    ProjectRepository projectRepository;
    
    /**
     * A slug should be valid with slugify. 
     * A slug should be unique for the current profile. 
     * So the current user doesn't have the same slug in his existing projects.
     */
    @Override
    public boolean isValid(
        UUID prjectId, ConstraintValidatorContext context
    ) {

        Profile profile = profileService.get();
        
        Project project = projectRepository.findById(prjectId).orElse(null);
        if (project == null) {
            context.buildConstraintViolationWithTemplate(
                "Project is not found."
            ).addConstraintViolation();

            return false;
        }

        if (!project.getProfileId().equals(profile.getProfileId())) {
            context.buildConstraintViolationWithTemplate(
                "Only the owner can update this project."
            ).addConstraintViolation();

            return false;
        }

        return true;
    }

}
