package com.ekotak.project.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.ekotak.project.domain.Profile;
import com.ekotak.project.domain.ProjectByProfile;
import com.ekotak.project.repositories.ProjectByProfileRepository;
import com.ekotak.project.services.ProfileService;
import com.github.slugify.Slugify;

import org.springframework.beans.factory.annotation.Autowired;

public class IsNewSlugValidator 
    implements ConstraintValidator<IsNewSlug, String> 
{

    @Autowired
    ProfileService profileService;

    @Autowired
    ProjectByProfileRepository projectByProfileRepository;
    
    /**
     * A slug should be valid with slugify. 
     * A slug should be unique for the current profile. 
     * So the current user doesn't have the same slug in his existing projects.
     */
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {

        if (value != null) {
            Slugify slug = new Slugify();
            String slugValue = slug.slugify(value);
            
            if (!value.equals(slugValue)) {
                context.buildConstraintViolationWithTemplate(
                    String.format(
                        "The slug %s is invalid. The valid is %s.", 
                        value, slugValue
                    ) 
                ).addConstraintViolation();
                
                return false;
            }

            Profile profile = profileService.get();
            ProjectByProfile projectByProfile = projectByProfileRepository
                .findByKeyProfileIdAndKeySlug(
                    profile.getProfileId(), value
                ).orElse(null);

            if (projectByProfile != null) {
                context.buildConstraintViolationWithTemplate(
                    "The slug is already taken in your other project."
                ).addConstraintViolation();

                return false;
            }
        }

        return true;
    }

}
