package com.ekotak.project.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = IsUpdateSlugValidator.class)
@Documented
public @interface IsUpdateSlug {

    String message() default "The slug is not valid.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}