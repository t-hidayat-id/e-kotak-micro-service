package com.ekotak.project.repositories;

import java.util.Optional;
import java.util.UUID;

import com.ekotak.project.domain.Project;

import org.springframework.data.repository.CrudRepository;

public interface ProjectRepository extends CrudRepository<Project, UUID> {
   
    Optional<Project> findByProjectId(UUID projectId);

}