package com.ekotak.project.repositories;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.ekotak.project.domain.ProjectByProfile;
import com.ekotak.project.domain.ProjectByProfileKey;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectByProfileRepository 
    extends CassandraRepository<ProjectByProfile, ProjectByProfileKey> 
{

    Optional<ProjectByProfile> findByKeyProfileIdAndKeySlug(
        UUID profileId, String slug
    );
    List<ProjectByProfile> findByKeyProfileId(UUID profileId);

}