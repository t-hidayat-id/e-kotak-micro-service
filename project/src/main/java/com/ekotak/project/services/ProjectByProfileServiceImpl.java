package com.ekotak.project.services;

import java.util.List;

import com.ekotak.project.domain.Profile;
import com.ekotak.project.domain.ProjectByProfile;
import com.ekotak.project.repositories.ProjectByProfileRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectByProfileServiceImpl implements ProjectByProfileService {

    ProfileService profileService;
    ProjectByProfileRepository projectByProfileRepository; 

    @Autowired
    public ProjectByProfileServiceImpl(
        ProfileService profileService, 
        ProjectByProfileRepository projectByProfileRepository
    ) {
        this.profileService = profileService;
        this.projectByProfileRepository = projectByProfileRepository;
    }

    @Override
    public List<ProjectByProfile> findAllByProfile() {
        Profile profile = this.profileService.get();
        return this.projectByProfileRepository.findByKeyProfileId(
            profile.getProfileId()
        );
    }

}