package com.ekotak.project.services;

import com.ekotak.project.domain.Project;

import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class ProjectProducerService {

    private final KafkaTemplate<Object, Project> kafkaTemplate;

    ProjectProducerService(KafkaTemplate<Object, Project> kafkaTemplate) {
		this.kafkaTemplate = kafkaTemplate;
	}

	public void send(Project project) {
		this.kafkaTemplate.send("projectTopic", project);
	}

}