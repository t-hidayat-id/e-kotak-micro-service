package com.ekotak.project.services;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import com.ekotak.project.domain.Profile;
import com.ekotak.project.domain.Project;
import com.ekotak.project.exceptions.ProjectNotFoundException;
import com.ekotak.project.repositories.ProjectRepository;
import com.fasterxml.uuid.Generators;
import com.github.slugify.Slugify;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectServiceImpl implements ProjectService {

    ProfileService profileService;
    ProjectRepository projectRepository;
    ProjectProducerService projectProducerService;

    @Autowired
    public ProjectServiceImpl(
        ProfileService profileService, 
        ProjectRepository projectRepository,
        ProjectProducerService projectProducerService
    ) {
        this.profileService = profileService;
        this.projectRepository = projectRepository;
        this.projectProducerService = projectProducerService;
    }

    @Override
    public Project create(Project project) {
        UUID projectId = Generators.timeBasedGenerator().generate();
        project.setProjectId(projectId);

        Profile profile = profileService.get();
        project.setProfileId(profile.getProfileId());

        if (project.getSlug() == null) {
            String projectDateTime = String.format(
                "project %s %s", LocalDate.now().toString(),
                LocalTime.now().format(
                    DateTimeFormatter.ofPattern("HH:mm")
                ).toString()
            );
            Slugify slug = new Slugify();
            String result = slug.slugify(projectDateTime);

            project.setSlug(result);
        }

        project = this.projectRepository.save(project);
        
        this.projectProducerService.send(project);

        return project;
    }

    @Override
    public Project update(Project project) {
        Profile profile = profileService.get();
        project.setProfileId(profile.getProfileId());
        
        project = this.projectRepository.save(project);
        return project;
    }

    @Override
    public Project findByProjectId(UUID projectId) {
        Project project = this.projectRepository.findByProjectId(
            projectId
        ).orElseThrow(() -> new ProjectNotFoundException(projectId));

        return project;
    }

}