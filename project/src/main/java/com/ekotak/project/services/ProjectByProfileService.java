package com.ekotak.project.services;

import java.util.List;

import com.ekotak.project.domain.ProjectByProfile;

public interface ProjectByProfileService {

    List<ProjectByProfile> findAllByProfile();

}