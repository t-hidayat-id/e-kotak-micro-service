package com.ekotak.project.services;

import java.util.UUID;

import com.ekotak.project.domain.Project;

public interface ProjectService {

    Project create(Project project);
    Project update(Project project);
    Project findByProjectId(UUID projectId);

}