package com.ekotak.project.services;

import com.ekotak.project.domain.Profile;

public interface ProfileService {

    Profile get();

}