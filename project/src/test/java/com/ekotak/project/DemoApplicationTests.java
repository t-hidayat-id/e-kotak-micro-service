package com.ekotak.project;

import com.ekotak.project.domain.ProjectByProfile;
import com.ekotak.project.domain.ProjectByProfileKey;
import com.ekotak.project.repositories.ProjectByProfileRepository;
import com.fasterxml.uuid.Generators;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

	@Autowired
	ProjectByProfileRepository projectByProfileRepository;

	@Test
	public void contextLoads() {
		ProjectByProfileKey projectByProfileKey = new ProjectByProfileKey(
			Generators.timeBasedGenerator().generate(),  "test"
		);
		ProjectByProfile projectByProfile = new ProjectByProfile("test", "test");
		projectByProfile.setKey(projectByProfileKey);

		projectByProfileRepository.save(projectByProfile);
	}

}
