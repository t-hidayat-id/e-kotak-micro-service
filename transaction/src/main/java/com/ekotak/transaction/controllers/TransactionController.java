package com.ekotak.transaction.controllers;

import javax.validation.Valid;

import com.ekotak.transaction.domain.Transaction;
import com.ekotak.transaction.models.TransactionPostModel;
import com.ekotak.transaction.services.TransactionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/transaction")
public class TransactionController {

    @Autowired
    TransactionService transactionService;

    @PostMapping(
        consumes = { 
            MediaType.APPLICATION_JSON_VALUE 
        },
		produces = { 
            MediaType.APPLICATION_JSON_VALUE 
        }
	)
    public ResponseEntity<Object> create(
        @Valid @RequestBody TransactionPostModel transactionPostModel
    ) {
        Transaction transaction = new Transaction(
            transationPostModel.gettransation(),
            currencyPostModel.getcurrency(),
            transationPostModel.gettransation(),
            valuePostModel.getvalue(),
            relatedPostModel.getrelated(),
            relatedIdPostModel.getrelatedId());

        return ResponseEntity.status(HttpStatus.CREATED).body(
            transactioinService.create(transaction)
        );
    }
}