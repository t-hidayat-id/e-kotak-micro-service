package com.ekotak.transaction.repositories;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.ekotak.transaction.domain.Transaction;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionRepository extends CrudRepository<Transaction, UUID> {
   
    Optional<Transaction> findByTransactionId(UUID transactionId);
    List<Transaction> findAll(); 

}