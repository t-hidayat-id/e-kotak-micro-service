package com.ekotak.transaction.domain;

import java.io.Serializable;
import java.util.UUID;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import lombok.Data;

@Data
@Table("transaction")
public class Transaction implements Serializable {

    private static final long serialVersionUID = -6055299955950900198L;

    @PrimaryKey("transaction_id")
    UUID transactionId;

    @Column("user_id")
    UUID userId;

    @Column("transantion")
    Type transantion;

    @Column("currency")
    String currency;

    @Column("value")
    BigDecimal value;

    @Column("related")
    Table related;

    @Column("related_id")
    UUID relatedId;

    public Transaction(UUID transactionId, UUID userId, Type transantion, String currency, BigDecimal value, Table related, UUID relatedId) {
        this.transactionId = transactionId;
        this.userId = userId;
        this.transantion = transantion;
        this.currency = currency;
        this.value = value;
        this.related = related;
        this.relatedId = relatedId;
    }

}