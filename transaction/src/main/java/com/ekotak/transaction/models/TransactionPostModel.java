package com.ekotak.transaction.models;

import java.math.BigDecimal;
import java.util.UUID;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class ProjectDonationPostModel {

    @NotNull(message = "Project Id cannot be null.")
    UUID projectId;

    @NotNull(message = "Transantion cannot be null.")
    Type transantion;

    @NotNull(message = "Currency cannot be null.")
    String currency;

    @NotNull(message = "Value cannot be null.")
    BigDecimal value;

    @NotNull(message = "Related cannot be null.")
    Table related;

    @NotNull(message = "Related Id cannot be null.")
    UUID relatedId;

}