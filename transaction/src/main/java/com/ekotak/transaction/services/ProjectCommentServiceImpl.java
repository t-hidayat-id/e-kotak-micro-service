package com.ekotak.projectcomment.services;

import java.util.UUID;

import com.ekotak.projectcomment.domain.Profile;
import com.ekotak.projectcomment.domain.ProjectComment;
import com.ekotak.projectcomment.repositories.ProjectCommentRepository;
import com.fasterxml.uuid.Generators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectCommentServiceImpl implements ProjectCommentService {

    ProjectCommentRepository projectCommentRepository;
    ProfileService profileService;

    @Autowired
    public ProjectCommentServiceImpl(ProjectCommentRepository projectCommentRepository) {
        this.projectCommentRepository = projectCommentRepository;
    }

    @Override
    public ProjectComment create(ProjectComment projectComment) {
        UUID projectCommentId = Generators.timeBasedGenerator().generate();
        projectComment.setProjectCommentId(projectCommentId);
        
        Profile profile = profileService.get();
        projectComment.setProfileId(profile.getProfileId());

        projectComment = projectCommentRepository.save(projectComment);
        return projectComment;
    }

    @Override
    public boolean delete(ProjectComment projectComment) {        
        projectCommentRepository.delete(projectComment);
        
        return true;
    }

    @Override
    public ProjectComment findByProjectCommentId(UUID projectCommentId) {
        ProjectComment projectComment = projectCommentRepository.findByProjectCommentId(
            projectCommentId
        ).orElse(null);

        return projectComment;
    }

}