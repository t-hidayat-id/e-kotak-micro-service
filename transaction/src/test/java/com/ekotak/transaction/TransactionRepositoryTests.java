package com.ekotak.transaction;

import static org.assertj.core.api.Assertions.*;

import java.util.List;
import java.util.UUID;

import com.ekotak.transaction.domain.Transaction;
import com.ekotak.transaction.repositories.TransactionRepository;
import com.fasterxml.uuid.Generators;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TransactionApplication.class)
public class TransactionRepositoryTests {

    @Autowired
    TransactionRepository transactionRepository;
    UUID transactionId;

    @Before
    public void setUp() {
        transactionId = Generators.timeBasedGenerator().generate();
        
        Transaction transaction = new Transaction(
            transactionId, 
            Generators.timeBasedGenerator().generate(), 
            Generators.timeBasedGenerator().generate(),
            "Like"
        );
    
        transactionRepository.save(transaction);
    }

    @Test
    public void findByTransaction() {
        Transaction transaction = transactionRepository.findByTransactionId(
            transactionId
        ).orElse(null);

        assertThat(transaction.getTransactionId().equals(transactionId)).isTrue();
    }

    @Test
    public void findAll() {
        List<Transaction> transactions = transactionRepository.findAll();

        assertThat(transactions.isEmpty()).isFalse();
    }

    @After
    public void setDone() {
        Transaction transaction = transactionRepository.findByTransactionId(
            transactionId
        ).orElse(null);

        transactionRepository.delete(transaction);
    }
}