package com.ekotak.transaction;

import static org.assertj.core.api.Assertions.*;

import java.util.UUID;

import com.ekotak.transaction.domain.Transaction;
import com.ekotak.transaction.services.TransactionService;
import com.fasterxml.uuid.Generators;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TransactionApplication.class)
public class TransactionServiceTest {

    @Autowired
    TransactionService transactionService;
    UUID transactionId;

    @Test
    public void createDeleteFind() {
        Transaction transaction = new Transaction(
           null, 
           Generators.timeBasedGenerator().generate(),
           Generators.timeBasedGenerator().generate(),
           "Like"
        );
        transaction = transactionService.create(transaction);

        transactionId = transaction.getTransactionId();
        assertThat(transactionId.toString().isEmpty()).isFalse();

        transactionService.delete(transaction);

        transaction = transactionService.findByTransactionId(transactionId);
        assertThat(transaction).isNull();
    }

}